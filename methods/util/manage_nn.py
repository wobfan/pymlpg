from __future__ import annotations

import typing

from methods.util.output_check import OutputCheck
from methods.neural_network.parameters import *

if typing.TYPE_CHECKING:
    from methods.neural_network.neuron import Neuron
    from methods.neural_network.deep_learning.neural_network_deeplearning import NeuralNetworkDeepLearning
    from methods.neural_network.deep_learning.output_neuron_deep_learning import OutputNeuronDeepLearning
    from methods.neural_network.deep_learning.hidden_neuron_deep_learning import HiddenNeuronDeepLearning
    from methods.util.labelled_points_and_boxes import LabelledPointsAndBoxes

from typing import List


def count_training_set_entries(training_set: List[LabelledPointsAndBoxes]):
    counted_entries = 0
    for labelled_points_and_boxes in training_set:
        counted_entries += len(labelled_points_and_boxes.labelled_points)
    return counted_entries


def train_nn(nn: NeuralNetworkDeepLearning, output_neuron: OutputNeuronDeepLearning,
             training_set: List[LabelledPointsAndBoxes], output_csv_file: str, print_to_log: bool):
    nn.compute_layers()
    nn.different_input_datasets = count_training_set_entries(training_set)

    iteration = 0

    while True:
        output_check = OutputCheck()
        net_error = 0.0

        for labelled_points_and_boxes in training_set:
            training_points: LabelledPointsAndBoxes = labelled_points_and_boxes
            for lp in training_points.labelled_points:
                net_error += nn.set_inputs(lp.x, lp.y).simulate().set_target_output(lp.neuron, lp.label).get_error(
                    lp.neuron)
                output_check.add_computed_and_to_train(
                    [lp.neuron.get_fired(), None if lp.label is None else lp.label == OUTPUT_TRUE])
            if print_to_log:
                # TODO
                pass
            nn.train(net_error)
        iteration += 1
        points_incorrectly_classified = output_check.all_non_equal_values()

        if not (iteration < TRAININGCYCLES and points_incorrectly_classified > 0):
            break

    # TODO add MATLAB

    result = [points_incorrectly_classified, net_error]
    return result


def determine_number_of_layers_and_neurons(layers_max, neurons_max, randomly):
    layers_to_create = random.randint(0, layers_max) if randomly else layers_max
    number_of_neurons_of_layer_x: List[int] = []
    for i in range(0, layers_to_create):
        number_of_neurons_of_layer_x.append(random.randint(0, neurons_max) if randomly else neurons_max)
    return number_of_neurons_of_layer_x


def increase_layers_max_neurons_max(layers_max_neurons_max: List[int]):
    if layers_max_neurons_max[1] < MAX_NEURONS_BRUTEFORCE:
        layers_max_neurons_max[1] += 1
        return False
    elif layers_max_neurons_max[0] < MAX_LAYERS_BRUTEFORCE:
        layers_max_neurons_max[0] += 1
        layers_max_neurons_max[1] = 1
        return False
    return True


def create_and_connect_hidden_neurons(nn: NeuralNetworkDeepLearning,
                                      input_neurons: List[Neuron],
                                      number_hidden_layers_and_neurons: List[int],
                                      output_neuron: OutputNeuronDeepLearning):
    previous_neurons_to_connect: List[Neuron] = []

    for i in range(0, len(number_hidden_layers_and_neurons)):
        neurons = number_hidden_layers_and_neurons[i]
        neurons_created: List[Neuron] = []
        for n in range(0, neurons):
            hidden_neuron = HiddenNeuronDeepLearning(nn, "H" + ":l" + str(i) + ":n" + str(n))
            neurons_created.append(hidden_neuron)
            connect_input_neurons(previous_neurons_to_connect, hidden_neuron)
        previous_neurons_to_connect = []
    connect_input_neurons(previous_neurons_to_connect, output_neuron)


def connect_input_neurons(previous_neurons_to_connect: List[Neuron], hidden_neuron: HiddenNeuronDeepLearning):
    for input_neuron in previous_neurons_to_connect:
        hidden_neuron.connect_input(input_neuron)
