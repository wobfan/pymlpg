class CSVReader:

    def __init__(self, fileName):
        try:
            self.__reader = open(str(fileName), "r")
        except FileNotFoundError:
            print(FileNotFoundError)

    def readLine(self):
        values = []

        for line in self.__reader.readlines():
            values.append(line.split(","))

        return values

    def close(self):
        try:
            self.__reader.close()
        except IOError:
            print(IOError)
