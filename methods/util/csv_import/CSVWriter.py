from methods.neural_network.neural_network import NeuralNetwork
from methods.neural_network.neuron import Neuron
from methods.neural_network.parameters import INPUT_FALSE, INPUT_TRUE, PLOT_INCREMENT, PRECISION


class CSVWriter:

    def __init__(self, fileName):
        self.__numberAttributes = 0

        try:
            self.__writer = open(fileName, "w")
        except FileExistsError:
            print(FileExistsError)

    def writeHeader(self, datasetName, attributeNames):

        self.__numberAttributes = len(attributeNames)
        attributeNamesString = ""

        for i, attribute in enumerate(attributeNames):

            attributeNamesString += str(attribute)

            if i + 1 < self.__numberAttributes:
                attributeNamesString += ","

        try:
            self.__writer.write("header, " + str(datasetName) + "\n" + attributeNamesString + "\n")

        except IOError:
            print(IOError)

    def writeLine(self, values):
        valuesString = ""

        for i, value in enumerate(values):
            valuesString += str(value)

            if (i + 1) < len(str(value)):
                valuesString += ", "

        valuesString += "\n"

        try:
            self.__writer.write(valuesString)
        except IOError:
            print(IOError)

    def writeData(self, nn: NeuralNetwork, o1: Neuron, label, wasFired):
        valuesWritten = 0

        self.writeHeader(label, ["input1", "input2", "output"])
        input1 = INPUT_FALSE
        while input1 <= INPUT_TRUE:
            input2 = INPUT_FALSE
            while input2 <= INPUT_TRUE:

                nn.set_inputs(input1, input2)
                nn.forward_propagation()
                # if (wasFired == null ||					    continuously firing neurons
                if o1.get_fired() == wasFired:  # discretely firing neurons

                    self.writeLine([input1, input2, o1.compute_output()])
                    valuesWritten += 1

                input2 = round((input2 + PLOT_INCREMENT) * PRECISION) / PRECISION

            input1 = round((input1 + PLOT_INCREMENT) * PRECISION) / PRECISION

        return str(valuesWritten)

    def close(self):
        self.__writer.close()
