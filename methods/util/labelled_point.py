from __future__ import annotations

import typing

# from methods.util.labelled_points_and_boxes import LabelledPointsAndBoxes
import \
    methods.util.labelled_points_and_boxes as labelled_points_and_boxes  # workaround the above from..import to avoid circular import
from methods.neural_network.parameters import *

if typing.TYPE_CHECKING:
    from methods.neural_network.neuron import Neuron


def generate_random_value_according_to_parameter_interval():
    value = random.uniform(0.0, 1.0)
    if INPUT_FALSE < 0:
        value *= -1 if random.choice([True, False]) else 1
    return value


class LabelledPoint:
    x = None
    y = None
    neuron: Neuron = None
    label: float = None

    def __init__(self, x, y, neuron: Neuron, label=None):
        self.neuron = neuron
        self.x = round(
            (generate_random_value_according_to_parameter_interval() if x is None else x) * PRECISION) / PRECISION
        self.y = round(
            (generate_random_value_according_to_parameter_interval() if y is None else y) * PRECISION) / PRECISION

        for b in labelled_points_and_boxes.LabelledPointsAndBoxes.boxes:
            self.label = OUTPUT_TRUE if b.x1 <= self.x <= b.x2 and b.y1 <= self.y <= b.y2 else OUTPUT_FALSE

        if label is not None:
            self.label = label
