from lxml import etree as XML
from typing import List
from methods.neural_network.neuron import Neuron
from methods.neural_network.neural_network import NeuralNetwork
from methods.util.xml.XMLReader import XMLReader


class XMLWriter:

    @staticmethod
    def __xmlAddNeuronList(parentElement, listName, neurons: List[Neuron]):
        listParentElement = XML.Element(listName)
        parentElement.append(listParentElement)
        for neuron in neurons:
            neuronElement = XML.Element("neuron")
            listParentElement.append(neuronElement)
            neuronElement.set("bias", str(neuron.get_bias()))
            neuronElement.set("fired", str(neuron.get_fired()))
            neuronElement.set("id", str(neuron.get_id()))
            neuronElement.set("output", str(neuron.compute_output()))

    @staticmethod
    def saveNetwork(neuronalNetwork: NeuralNetwork, fileName):
        rootElement = XML.Element("neuralNetwork")

        element1 = XML.Element("lastTraining")
        element2 = XML.Comment("TO BE DONE")
        element1.append(element2)
        rootElement.append(element1)

        neuronsElement = XML.Element("neurons")
        rootElement.append(neuronsElement)
        XMLWriter.__xmlAddNeuronList(neuronsElement, "inputNeurons", neuronalNetwork.input_neurons)
        XMLWriter.__xmlAddNeuronList(neuronsElement, "hiddenNeurons", neuronalNetwork.hidden_neurons)
        # XMLWriter.__xmlAddNeuronList(neuronsElement, "hiddenOutputNeurons", neuronalNetwork.hidden_output_neurons)
        XMLWriter.__xmlAddNeuronList(neuronsElement, "outputNeurons", neuronalNetwork.output_neurons)

        connectionsElement = XML.Element("connectionsElement")
        rootElement.append(connectionsElement)

        for k, v in neuronalNetwork.input_synapses.items():
            connectionElement = XML.Element("connection")
            connectionElement.set("neuron", str(k.get_id()))
            connectionsElement.append(connectionElement)

            for neuron, weight in v.items():
                inputNeuronElement = XML.Element("inputNeuron")
                inputNeuronElement.set("neuron", str(neuron.get_id()))
                inputNeuronElement.set("weight", str(weight))
                connectionElement.append(inputNeuronElement)

        tree = XML.ElementTree(rootElement)
        XML.indent(tree, space="    ")
        with open(fileName, "wb") as files:
            tree.write(files, encoding='utf-8', xml_declaration=True, pretty_print=True)

        verification = XMLWriter._verify(neuronalNetwork, fileName)

        if not verification:
            print("Something went wrong while creating the file")

    @staticmethod
    def _verify(nn: NeuralNetwork, filename):

        neural_network: NeuralNetwork = XMLReader.loadNetwork(filename)
        if neural_network.input_neurons != nn.input_neurons:
            if neural_network.hidden_neurons == nn.hidden_neurons:
                if neural_network.output_neurons == nn.output_neurons:
                    return True
                else:
                    return False
            else:
                return False
        else:
            return False
