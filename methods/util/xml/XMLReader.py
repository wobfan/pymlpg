from methods.neural_network.hill_climbing.hidden_neuron_hill_climbing import HiddenNeuronHillClimbing
from methods.neural_network.hill_climbing.output_neuron_hill_climbing import OutputNeuronHillClimbing
from methods.neural_network.hill_climbing.neural_network_hill_climbing import NeuralNetworkHillClimbing
from methods.neural_network.input_neuron import InputNeuron
from typing import Dict
from methods.neural_network.neuron import Neuron
from methods.util.xml.XMLConstants import *
from xml.dom import minidom
from methods.neural_network import parameters


class XMLReader:

    @staticmethod
    def loadNetwork(fileName: str):
        neuralNetwork = NeuralNetworkHillClimbing()
        nnNeurons: Dict[str, Neuron] = dict()

        root_element = minidom.parse(fileName)
        neurons = root_element.getElementsByTagName(XML_NEURONS).item(0)

        XMLReader.__add_input_neurons(neuralNetwork, nnNeurons, neurons)
        XMLReader.__add_hidden_neurons(neuralNetwork, nnNeurons, neurons)
        XMLReader.__add_output_neurons(neuralNetwork, nnNeurons, neurons)
        XMLReader.__add_connections(nnNeurons, root_element)

        return neuralNetwork

    @staticmethod
    def __add_connections(nnNeurons, rootElement):
        connections = rootElement.getElementsByTagName(XML_CONNECTIONS_ELEMENT)[0]

        for connection in XMLReader.__extract_element_iterator(connections):
            neuron: Neuron = nnNeurons.get(connection.getAttribute(XML_NEURON))

            for inputConnection in XMLReader.__extract_element_iterator(connection):
                neuron.connect_input(nnNeurons.get(inputConnection.getAttribute(XML_NEURON)), float(inputConnection.getAttribute(XML_WEIGHT)))

    @staticmethod
    def __add_output_neurons(neuralNetwork, nnNeurons, neurons):
        outputNeurons = neurons.getElementsByTagName(XML_OUTPUT_NEURONS)[0]

        for output_neuron in XMLReader.__extract_element_iterator(outputNeurons):
            neuron = OutputNeuronHillClimbing(neuralNetwork, output_neuron.getAttribute(XML_ID)).bias(float(output_neuron.getAttribute(XML_BIAS)))
            nnNeurons[neuron.id] = neuron

    @staticmethod
    def __add_hidden_neurons(neuralNetwork, nnNeurons, neurons):
        hidden_neurons = neurons.getElementsByTagName(XML_HIDDEN_NEURONS)[0]

        for hidden_neuron in XMLReader.__extract_element_iterator(hidden_neurons):
            neuron = HiddenNeuronHillClimbing(neuralNetwork, hidden_neuron.getAttribute(XML_ID)).bias(float(hidden_neuron.getAttribute(XML_BIAS)))
            nnNeurons[neuron.id] = neuron

    @staticmethod
    def __add_input_neurons(neuralNetwork, nnNeurons, neurons):
        input_neurons = neurons.getElementsByTagName(XML_INPUT_NEURONS)[0]

        for input_neuron in XMLReader.__extract_element_iterator(input_neurons):
            neuron: Neuron = InputNeuron(neuralNetwork, input_neuron.getAttribute(XML_ID), parameters.INPUT_TRUE)
            nnNeurons[neuron.id] = neuron

    @staticmethod
    def __extract_element_iterator(parent_element):
        child_nodes = parent_element.childNodes
        element_list = []
        for child in child_nodes:

            if child.nodeType == minidom.Node.ELEMENT_NODE:
                element_list.append(child)

        return element_list
