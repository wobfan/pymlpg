from methods.util.csv_import.CSVReader import CSVReader
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns


def scatter2D(filename):
    FIRST_ATTRIBUTE_INDEX = 0
    SECOND_ATTRIBUTE_INDEX = 1

    list = []

    reader = CSVReader(filename)
    values = reader.readLine()
    reader.close()

    for x in values:
        if x[FIRST_ATTRIBUTE_INDEX] == "header":
            header = x[SECOND_ATTRIBUTE_INDEX]
            header = header[:-1]

        if x[FIRST_ATTRIBUTE_INDEX] != "input1" and x[FIRST_ATTRIBUTE_INDEX] != "header":
            data_to_add = {'x': x[FIRST_ATTRIBUTE_INDEX], 'y': x[SECOND_ATTRIBUTE_INDEX], 'label': header}
            list.append(data_to_add)

    data = pd.DataFrame(list)

    sns.scatterplot(x="x", y="y", data=data, hue='label', style='label', s=100)
    plt.suptitle('Scatter Chart 2D')
    plt.title('NN')
    plt.xlabel('X')
    plt.ylabel('Y')
    plt.tick_params(axis='x', which='major', labelsize=4)
    plt.tick_params(axis='y', which='major', labelsize=4)
    plt.show()
