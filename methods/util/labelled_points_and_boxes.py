from __future__ import annotations

from math import sqrt
from typing import List

from methods.util.box import Box
from methods.neural_network.parameters import *
# from methods.util.labelled_point import LabelledPoint
import methods.util.labelled_point as labelled_point  # workaround the above from..import to avoid circular import


class LabelledPointsAndBoxes:
    boxes: List[Box] = []

    def __init__(self, number=None, n=None, equidistant=False):
        self.labelled_points: List[labelled_point.LabelledPoint] = []
        if number is not None and n is not None:
            if equidistant:
                number_of_increments = sqrt(number)
                increment = abs(INPUT_TRUE - INPUT_FALSE) / (number_of_increments - 1)

                x = INPUT_FALSE
                while x < INPUT_TRUE:
                    y = INPUT_FALSE
                    while y < INPUT_TRUE:
                        self.labelled_points.append(labelled_point.LabelledPoint(x, y, n))
                        y += increment
                    x += increment
            else:
                for i in range(0, number):
                    self.labelled_points.append(labelled_point.LabelledPoint(None, None, n))
