from typing import List


class TwoValues:
    def __init__(self, value_computed, value_trained):
        self.value_computed = value_computed
        self.value_trained = value_trained

    def check_if_equal(self):
        if self.value_computed == self.value_trained:
            return True

        if self.value_computed is None:
            return False

        return self.value_computed is self.value_trained


class OutputCheck:
    def __init__(self):
        self.value_list: List[TwoValues] = []

    def add_computed_and_to_train(self, computed, to_train):
        self.value_list.append(TwoValues(computed, to_train))

    def all_non_equal_values(self):
        non_equal_values = 0
        for tv in self.value_list:
            if not tv.check_if_equal():
                non_equal_values += 1
        return non_equal_values
