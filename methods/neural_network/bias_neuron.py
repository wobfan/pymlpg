from __future__ import annotations

import typing

from methods.neural_network.neuron import Neuron

if typing.TYPE_CHECKING:
    from methods.neural_network.neural_network import NeuralNetwork


class BiasNeuron(Neuron):

    def __init__(self, nn: NeuralNetwork, id):
        super().__init__(nn, id)

    def train(self):
        # intentionally left blank
        pass

    def undo_training(self):
        # intentionally left blank
        pass

    def compute_output(self):
        return 1.0
