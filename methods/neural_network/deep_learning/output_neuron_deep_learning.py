from __future__ import annotations

import typing

from methods.neural_network.deep_learning.hidden_neuron_deep_learning import HiddenNeuronDeepLearning
from methods.neural_network.deep_learning.neuron_deep_learning import *

if typing.TYPE_CHECKING:
    from methods.neural_network.deep_learning.neural_network_deeplearning import NeuralNetworkDeepLearning


class OutputNeuronDeepLearning(HiddenNeuronDeepLearning, NeuronDeepLearning):

    def __init__(self, nn: NeuralNetworkDeepLearning, id):
        super().__init__(nn, id, False)
        self.nn.output_neurons.append(self)
