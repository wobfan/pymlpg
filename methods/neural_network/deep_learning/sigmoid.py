from methods.neural_network.deep_learning.activation_function import ActivationFunction

import math


class Sigmoid(ActivationFunction):

    def compute(self, value):
        return 1 / (1 + math.exp(-value))

    def compute_derivation_of_node(self, value):
        return value * (1 - value)
