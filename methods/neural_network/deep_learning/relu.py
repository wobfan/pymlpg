from .activation_function import ActivationFunction
import math


class RELU(ActivationFunction):

    def compute(self, value):
        return value if value > 0 else 0

    def compute_derivation_of_node(self, value):
        return 1 if value > 0 else 0
