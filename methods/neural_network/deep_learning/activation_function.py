from abc import ABC, abstractmethod


class ActivationFunction(ABC):

    @abstractmethod
    def compute(self, value):
        pass

    @abstractmethod
    def compute_derivation_of_node(self, value):
        pass

    def compute_delta_output_layer(self, partial_error, out, input_weight):
        return (out - partial_error) * self.compute_derivation_of_node(out) * input_weight

    def compute_delta_hidden_layer(self, partial_error, out, input_weight):
        return partial_error * self.compute_derivation_of_node(out) * input_weight
