from .activation_function import ActivationFunction

import math


class Tanh(ActivationFunction):

    def compute(self, value):
        return math.tanh(value)

    def compute_derivation_of_node(self, value):
        return 1.0 - self.compute(value) ** 2
