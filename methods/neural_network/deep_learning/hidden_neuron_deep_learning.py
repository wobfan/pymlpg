from __future__ import annotations

import typing
from typing import Dict, List

from methods.neural_network.hill_climbing.hidden_neuron_hill_climbing import HiddenNeuronHillClimbing
from methods.neural_network.parameters import *
from methods.neural_network.deep_learning.neuron_deep_learning import *

if typing.TYPE_CHECKING:
    from methods.neural_network.neuron import Neuron
    from methods.neural_network.neural_network import NeuralNetwork


class HiddenNeuronDeepLearning(HiddenNeuronHillClimbing, NeuronDeepLearning):

    def __init__(self, nn: NeuralNetwork, id, add_to_hidden_neurons=True):
        super().__init__(nn, id, add_to_hidden_neurons)
        self.dl_nn = nn
        self.activation_function = None

    def act_function(self, activation_function):
        self.activation_function = activation_function
        return self

    def forward_propagation(self):
        error_margin_node = self.dl_nn.get_error_margin_node()
        self.output = self.compute_output()

        if OUTPUT_FALSE <= self.output <= OUTPUT_FALSE + error_margin_node:
            self.fired = False
        elif OUTPUT_TRUE - error_margin_node <= self.output <= OUTPUT_TRUE:
            self.fired = True
        else:
            self.fired = None

    def compute_output(self):
        self.output = 0.0
        input_neurons_2_weights: Dict[Neuron, float] = self.nn.input_synapses[self]

        for input in input_neurons_2_weights.keys():
            self.output += input.get_output() * input_neurons_2_weights[input]

        if self.activation_function is not None:
            self.output = self.activation_function.compute(self.output)
        elif self.dl_nn.default_activation_function is not None:
            self.output = self.dl_nn.default_activation_function.compute(self.output)

        return self.output

    def connect_and_or_change_weight(self, input_neuron: Neuron, weight):
        self.nn.add_or_change_synapse(input_neuron, self, weight)
