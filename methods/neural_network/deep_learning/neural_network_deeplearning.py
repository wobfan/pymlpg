from __future__ import annotations

import logging
import math
import typing

from methods.neural_network.neural_network import NeuralNetwork
from methods.neural_network.deep_learning.hidden_neuron_deep_learning import HiddenNeuronDeepLearning
from methods.neural_network.parameters import *
from methods.neural_network.deep_learning.neuron_deep_learning import *

if typing.TYPE_CHECKING:
    from methods.neural_network.neuron import Neuron
    from methods.neural_network.deep_learning.activation_function import ActivationFunction

from typing import List, Dict


class NeuralNetworkDeepLearning(NeuralNetwork):
    LITTLE_LARGER_ERROR_MARGIN = 1.1

    def __init__(self, default_activation_function: ActivationFunction, testing_lambda=None):
        super().__init__()
        self.default_activation_function = default_activation_function
        self.__lambda = LAMBDA if testing_lambda is None else testing_lambda

    def train(self, total_error_last_layer):
        neurons_to_process: List[Neuron] = self.all_neuron_layers[len(self.all_neuron_layers) - 1]
        is_output_layer = True

        while True:
            neurons_to_process_next: List[Neuron] = []
            for neuron in neurons_to_process:
                if neuron not in self.input_synapses:
                    continue
                input_neurons: Dict[Neuron, float] = self.input_synapses[neuron]
                for input_neuron in input_neurons.keys():
                    input_neuron.output_to_train = 0.0
                    if isinstance(input_neuron, HiddenNeuronDeepLearning) \
                            and input_neuron not in neurons_to_process_next:    # skip if already on the list to process
                        neurons_to_process_next.append(input_neuron)

            for neuron in neurons_to_process:
                self._train(neuron.output_to_train, neuron, is_output_layer)

            is_output_layer = False
            neurons_to_process = neurons_to_process_next.copy()

            if len(neurons_to_process) <= 0:
                break

        return total_error_last_layer

    def _train(self, partial_loss, neuron: Neuron, is_output_layer):
        out = neuron.get_output()
        if neuron not in self.input_synapses:
            return
        input_neurons: Dict[Neuron, float] = self.input_synapses[neuron]

        if isinstance(neuron, NeuronDeepLearning) and neuron.activation_function is not None:
            activation_function = neuron.activation_function
        else:
            activation_function = self.default_activation_function

        for input_neuron in input_neurons.keys():
            weight = input_neurons[input_neuron]

            """
            can't modify partial_loss (directly) because it may be used for a set of input neurons
            (partial_loss - out) is the first delta component (= target - out) of an output neuron
            output_to_train is the local error e.g., of a hidden neuron successively propagated during backpropagation
            """
            loss = out - partial_loss if is_output_layer else partial_loss
            gradient = activation_function.compute_derivation_of_node(out)
            input = input_neuron.get_output()

            # delta rule computing new weight
            delta = loss * gradient * input
            weight -= self.__lambda * delta

            # change trained weight
            self.add_or_change_synapse(input_neuron, neuron, weight)

            # propagate error to input node (with - already trained! - weight) & building error sum
            output_to_train_change = loss * gradient * weight
            input_neuron.set_output_to_train(input_neuron.get_output_to_train() + output_to_train_change)

    def get_error(self, output_neuron: Neuron):
        if output_neuron.output_to_train is None:
            if output_neuron.fired is not None:

                if output_neuron.get_fired():
                    new_training_value = output_neuron.get_output() \
                        - (OUTPUT_TRUE - self.LITTLE_LARGER_ERROR_MARGIN * self.get_error_margin_node())
                else:
                    new_training_value = (OUTPUT_TRUE + self.LITTLE_LARGER_ERROR_MARGIN
                                          * self.get_error_margin_node()) - output_neuron.get_output()

                output_neuron.output_to_train = new_training_value
                return new_training_value
            else:
                output_neuron.output_to_train = output_neuron.get_output()
                return 0.0
        else:
            return self.compute_half_squared_error(output_neuron.output_to_train - output_neuron.get_output())

    def get_error_margin_node(self):
        if self.size_input_dataset == 0:
            logging.critical("Different Data Input Sets not set! Resulting in infinite small node error margin.")
        return math.sqrt(ERROR_MARGIN_NET / self.size_input_dataset * 2 / len(self.output_neurons))
