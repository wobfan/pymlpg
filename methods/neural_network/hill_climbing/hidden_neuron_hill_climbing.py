from __future__ import annotations

import typing
from typing import Dict

from methods.neural_network.neuron import Neuron
from methods.neural_network.parameters import *
from methods.neural_network.input_neuron import InputNeuron

if typing.TYPE_CHECKING:
    from methods.neural_network.neural_network import NeuralNetwork


class HiddenNeuronHillClimbing(Neuron):

    def __init__(self, nn: NeuralNetwork, id, add_to_hidden_neurons=True):
        super().__init__(nn, id)
        self.bias_copy = None
        self.input_2_weights_copy: Dict[Neuron, float] = None
        self.add_to_neuron_sets(add_to_hidden_neurons)

    def add_to_neuron_sets(self, add_to_hidden_neurons):
        if add_to_hidden_neurons is True:
            self.nn.hidden_neurons.append(self)
        self.nn.hidden_output_neurons.append(self)

    def train(self):
        inputs_2_weights = self.nn.input_synapses[self]
        inputs_2_weights_copy = inputs_2_weights.copy()
        inputs_2_weights.clear()

        for input in inputs_2_weights_copy.keys():
            old_weight = inputs_2_weights_copy[input]
            change = RANDOM.random() * LAMBDA

            new_weight = self.restrict_value(old_weight + (change if RANDOM.choice([True, False]) else -change))
            inputs_2_weights[input] = new_weight

    def undo_training(self):
        input_neurons_2_weights = self.input_2_weights_copy.copy()
        self.nn.input_synapses[self] = input_neurons_2_weights
        self.input_2_weights_copy.clear()

    def compute_output(self):
        output = 0.0
        input_neurons_2_weights = self.nn.input_synapses[self]

        for input in input_neurons_2_weights.keys():
            if input.get_fired():
                output += (input.get_output() * input_neurons_2_weights[input]) if isinstance(input, InputNeuron) \
                    else input_neurons_2_weights[input]
        self.output = self.restrict_value(output)
        return self.output
