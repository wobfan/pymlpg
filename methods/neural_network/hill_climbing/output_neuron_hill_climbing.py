import typing
from methods.neural_network.hill_climbing.hidden_neuron_hill_climbing import HiddenNeuronHillClimbing

if typing.TYPE_CHECKING:
    from methods.neural_network.hill_climbing.hidden_neuron_hill_climbing import HiddenNeuronHillClimbing


class OutputNeuronHillClimbing(HiddenNeuronHillClimbing):

    def __init__(self, nn, id):
        super().__init__(nn, id, False)
        nn.output_neurons.append(self)
