import typing
from methods.neural_network.neuron import Neuron
from methods.neural_network.neural_network import NeuralNetwork
from methods.neural_network.parameters import *

if typing.TYPE_CHECKING:
    from methods.neural_network.neuron import Neuron


class NeuralNetworkHillClimbing(NeuralNetwork):

    def __init__(self):
        super().__init__()
        self.neurons_trained: typing.List[Neuron] = []
        self.previous_error = None

    def train(self, error):
        if self.previous_error is None or error <= self.previous_error:
            self.neurons_trained.clear()

            for neuron in self.hidden_output_neurons:
                if RANDOM.uniform(0, 1) < HILL_CLIMBING_NEURON_TRAINING_PROBABILITY:
                    neuron.train()
                    self.neurons_trained.append(neuron)

            self.previous_error = error

        else:
            self.undo_training
            self.previous_error = None

        return error

    def undo_training(self):
        for neuron in self.neurons_trained:
            neuron.undo_training()
