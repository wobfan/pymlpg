import typing
from methods.neural_network.hill_climbing.hidden_neuron_hill_climbing_continuous import HiddenNeuronHillClimbingContinuous

if typing.TYPE_CHECKING:
    from methods.neural_network.neural_network import NeuronalNetwork
    from methods.neural_network.hill_climbing.hidden_neuron_hill_climbing_continuous import HiddenNeuronHillClimbingContinuous


class OutputNeuronHillClimbingContinuous(HiddenNeuronHillClimbingContinuous):

    def __init__(self, nn: NeuronalNetwork, name, bias=None):

        super().__init__(nn, id, False)
        nn.output_neurons.append(self)
