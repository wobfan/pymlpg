from typing import Dict
import typing
from methods.neural_network.hill_climbing.hidden_neuron_hill_climbing import HiddenNeuronHillClimbing

if typing.TYPE_CHECKING:
    from methods.neural_network.neural_network import NeuronalNetwork
    from methods.neural_network.input_neuron import Neuron
    from methods.neural_network.hill_climbing.hidden_neuron_hill_climbing import HiddenNeuronHillClimbing


class HiddenNeuronHillClimbingContinuous(HiddenNeuronHillClimbing):

    def __init__(self, nn: NeuronalNetwork, name, add_to_hidden_neurons=True):
        if add_to_hidden_neurons is False:
            super.__init__(nn, name, add_to_hidden_neurons)
        else:
            super.__init__(nn, name)

    def get_output(self):
        output = 0.0
        input_Neurons_2_weights: Dict[Neuron, float] = self.nn.input_synapses[self]

        for input in input_Neurons_2_weights.keys():
            output += input.get_output() * input_Neurons_2_weights.get(input)

        output = self.restrict_value(output)

        return output
