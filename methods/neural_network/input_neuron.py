from __future__ import annotations

import typing

from methods.neural_network.neuron import Neuron

if typing.TYPE_CHECKING:
    from methods.neural_network.neural_network import NeuralNetwork


class InputNeuron(Neuron):
    def __init__(self, nn: NeuralNetwork, id, input=None):
        super().__init__(nn, id)
        nn.input_neurons.append(self)
        self.input: int = None
        if input is not None:
            self.change_input(input)

    def train(self):
        # intentionally left blank
        pass

    def undo_training(self):
        # intentionally left blank
        pass

    def compute_output(self):
        return self.input

    def change_input(self, input):
        self.input = input
        self.output = input

    def get_fired(self):
        return True

    def str(self, firing_details):
        if firing_details:
            return str(self) + "=" + str(self.input)
        else:
            return str(self)

    def __str__(self):
        return self.id
