from __future__ import annotations

import logging
import math
import typing
from typing import List, Dict, Set

from methods.neural_network.neuron import Neuron

if typing.TYPE_CHECKING:
    from methods.neural_network.cnn.neural_network_convolution import NeuralNetworkConvolution


class ActivationMapNeuron(Neuron):

    def __init__(self, cnn: NeuralNetworkConvolution, name, bias: float, filterNum: int, NeuronNum: int, ConvLayer: int,
                 inputDepth: int):
        super().__init__(cnn, name)
        if bias is not None:
            self.bias(bias)

        self.filterNum = filterNum
        self.NeuronNum = NeuronNum
        self.Layer = ConvLayer
        self.inputDepth = inputDepth
        self.convolution_nn = cnn
        self.outputBeforeSoftmax: float = 0
        self.d_L_d_input: float = 0
        self.bestNeuron: int = 0
        self.softMaxedOutPut: float = 0
        self.imAZero: int = 0

    def compute_output(self):
        self.output = 0.0
        inputNeurons2Weights: Dict[Neuron, float] = self.nn.input_synapses[self]

        for input in inputNeurons2Weights.keys():
            self.output += input.get_output() * inputNeurons2Weights[input]

        if self.output <= 0:
            self.output = 0.0
            self.imAZero = 0
        else:
            self.imAZero = 1

        return self.output

    def connect_and_or_change_weight(self, input_neuron: Neuron, weight: float):
        self.nn.add_or_change_synapse(input_neuron, self, weight)

    def train(self):
        pass

    def undo_training(self):
        pass
