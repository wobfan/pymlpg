from __future__ import annotations

import logging
import math
import typing
from typing import List, Dict, Set

from methods.neural_network.neuron import Neuron

if typing.TYPE_CHECKING:
    from methods.neural_network.neural_network import NeuralNetwork


class PaddingNeurons(Neuron):

    def __init__(self, nn: NeuralNetwork, name, bias: float):
        super().__init__(nn, name)
        if bias is not None:
            self.bias(bias)

    def compute_output(self):
        output = 0.0
        return output

    def train(self):
        pass

    def undo_training(self):
        pass
