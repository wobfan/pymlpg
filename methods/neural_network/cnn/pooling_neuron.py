from __future__ import annotations

import logging
import math
import typing
from typing import List, Dict, Set

from methods.neural_network.neuron import Neuron

if typing.TYPE_CHECKING:
    from methods.neural_network.cnn.neural_network_convolution import NeuralNetworkConvolution


class PoolingNeuron(Neuron):

    def __init__(self, cnn: NeuralNetworkConvolution, name, bias: float, filterNum: int, NeuronNum: int,
                 PoolingLayer: int, inputDepth: int):
        super().__init__(cnn, name)
        if bias is not None:
            self.bias(bias)

        self.filterNum = filterNum
        self.NeuronNum = NeuronNum
        self.Layer = PoolingLayer
        self.inputDepth = inputDepth
        cnn.pooling_neurons.append(self)
        self.convolution_nn = cnn
        self.softMaxedOutPut = 0
        self.bestNeuron = 0
        self.d_L_d_input = 0.0
        self.outputBeforeSoftmax = 0.0

    def compute_output(self):
        self.output = 0.0
        max: float = 0.0
        bestNeuron: List[Neuron] = []
        firstIteration = True
        inputNeuron2Weights: Dict[Neuron, float] = self.nn.input_synapses[self]

        for input in inputNeuron2Weights.keys():
            if firstIteration:
                max = input.get_output()
                firstIteration = False
            if input.get_output() >= max:
                max = input.get_output()
                if not bestNeuron:
                    bestNeuron.append(input)
                else:
                    bestNeuron[0] = input

        for input in inputNeuron2Weights.keys():
            if input == bestNeuron[0]:
                input.bestNeuron = 1
            else:
                input.bestNeuron = 0
        output = max
        return output

    def sumd_L_d_input(self, a: float):
        self.d_L_d_input += a

    def train(self):
        pass

    def undo_training(self):
        pass
