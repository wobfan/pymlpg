from __future__ import annotations

import logging
import math
import typing
from typing import List, Dict, Set

from methods.neural_network.cnn import activation_map_neuron, pooling_neuron
from methods.neural_network.neural_network import NeuralNetwork
from methods.neural_network.parameters import *

if typing.TYPE_CHECKING:
    from methods.neural_network.neuron import Neuron


class TrainSet:

    def __init__(self, INPUT_SIZE: int, OUTPUT_SIZE: int):
        self.INPUT_SIZE = INPUT_SIZE
        self.OUTPUT_SIZE = OUTPUT_SIZE
        self.data: List[List[List[float]]] = []

    def addData(self, inValue: List[float], expected: List[float]):
        if (len(inValue) != self.INPUT_SIZE | len(expected) != self.OUTPUT_SIZE):
            self.data.append([inValue, expected])
        return

    def getDataSize(self):
        return len(self.data)

    def getInput(self, index: int):
        if index >= 0 & index < len(self.data):
            a = self.data[index][0]
            return a
        else:
            return None

    def getOutput(self, index: int):
        if index >= 0 & index < len(self.data):
            return self.data[index][1]
        else:
            return None
