from __future__ import annotations

import logging
import math
import typing
from typing import List, Dict, Set


class RegionsHelper:

    def createRegionsConvolutionalLayer(self, oldHeight: int, oldWidth: int, filterHeight: int, filterWidth: int,
                                        numOfPads: int):

        regions: List[int][int] = []
        neuronPlacementMatrix: List[int][int] = [[0] * oldWidth for i in range(oldHeight)]
        newHeight: int = oldHeight - filterHeight + (2 * numOfPads) + 1
        newWidth: int = oldWidth - filterWidth + (2 * numOfPads) + 1

        regionsAll: List[List[int][int]] = []

        counter: int = 0
        for i in range(0, oldHeight):
            for j in range(0, oldWidth):
                neuronPlacementMatrix[i][j] = counter
                counter += 1

        neuronPlacementMatrix = self.padArray(neuronPlacementMatrix, -1, numOfPads)

        # print(neuronPlacementMatrix)

        for i in range(0, newHeight):
            for j in range(0, newWidth):
                regions = self.getSlice(neuronPlacementMatrix, i, i + filterHeight, j, j + filterWidth)
                regionsAll.append(regions)

        # print(regionsAll)
        return regionsAll

    def createRegionsPoolingLayer(self, oldHeight: int, oldWidth: int):
        regions: List[int][int] = []
        neuronPlacementMatrix: List[int][int] = [[0] * oldWidth for _ in range(oldHeight)]
        newHeight: int = oldHeight // 2
        newWidth: int = oldWidth // 2
        counter: int = 0

        for i in range(0, oldHeight):
            for j in range(0, oldWidth):
                neuronPlacementMatrix[i][j] = counter
                counter += 1

        regionsAll: List[List[int][int]] = []
        for j in range(0, newHeight):
            for k in range(0, newWidth):
                regions = self.getSlice(neuronPlacementMatrix, j * 2, j * 2 + 2, k * 2, k * 2 + 2)
                regionsAll.append(regions)

        return regionsAll

    def getSlice(self, array: List[int][int], startIndexI: int, endIndexI: int, startIndexJ: int, endIndexJ: int):
        slicedArray: List[int][int] = [[0] * (endIndexJ - startIndexJ) for i in range(endIndexI - startIndexI)]

        for i in range(0, len(slicedArray)):
            for j in range(0, len(slicedArray[i])):
                slicedArray[i][j] = array[startIndexI + i][startIndexJ + j]

        return slicedArray

    def padArray(self, arr: List[int][int], padWidth: int, numOfPads: int):
        temp: List[int][int] = [[0] * (len(arr[0]) + numOfPads * 2) for i in range((len(arr) + numOfPads * 2))]
        for i in range(0, len(temp)):
            for j in range(len(temp[i])):
                temp[i][j] = padWidth
        for i in range(0, len(arr)):
            for j in range(0, len(arr[i])):
                temp[i + numOfPads][j + numOfPads] = arr[i][j]
        return temp
