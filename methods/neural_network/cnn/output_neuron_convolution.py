from __future__ import annotations

import logging
import math
import typing
from typing import List, Dict, Set

from methods.neural_network.neuron import Neuron

if typing.TYPE_CHECKING:
    from methods.neural_network.cnn.neural_network_convolution import NeuralNetworkConvolution


class OutputNeuronConvolution(Neuron):

    def __init__(self, cnn: NeuralNetworkConvolution, name, bias: float, filterNum: int, NeuronNum: int,
                 ConvLayer: int, inputDepth: int):
        super().__init__(cnn, name, )
        if bias is not None:
            self.bias(bias)

        self.filterNum = filterNum
        self.NeuronNum = NeuronNum
        self.Layer = ConvLayer
        self.inputDepth = inputDepth
        cnn.output_neurons.append(self)
        self.imAZero = 0
        self.outputBeforeSoftmax = 0.0
        self.softMaxedOutPut = 0
        self.convolution_nn = cnn
        self.bestNeuron = 0
        self.d_L_d_input = 0.0

    def compute_output(self):
        self.output = 0.0
        inputNeurons2Weights: Dict[Neuron, float] = self.nn.input_synapses[self]

        for input in inputNeurons2Weights.keys():
            self.output += input.get_output() * inputNeurons2Weights[input]

        return self.output

    def train(self):
        pass

    def undo_training(self):
        pass
