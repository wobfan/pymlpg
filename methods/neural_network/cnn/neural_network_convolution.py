from __future__ import annotations

import math
import typing
from typing import List, Dict, Set

from methods.neural_network.cnn.activation_map_neuron import ActivationMapNeuron
from methods.neural_network.cnn.pooling_neuron import PoolingNeuron
from methods.neural_network.neural_network import NeuralNetwork
from methods.neural_network.parameters import *

if typing.TYPE_CHECKING:
    from methods.neural_network.neuron import Neuron


class NeuralNetworkConvolution(NeuralNetwork):

    def __init__(self, num_of_conv_layer):
        super().__init__()
        self.num_of_conv_layer = num_of_conv_layer
        self.sum_totals: float = 0
        self.t_exp_at_correct_class: float = 0
        self.gradient = 0
        self.acc: int = 0
        self.num_of_conv_layer: int = 0
        self.delta_filter: List[List[List[float]]] = []
        self.activation_map_neurons: List[List[List[Neuron]]] = []
        self.pooling_neurons: List[Neuron] = []
        self.all_filter: List[List[List[List[float]]]] = []
        self.softmax_array: List[float] = [0] * 10

    def train(self, error):
        neurons_to_process: List[Neuron] = self.all_neuron_layers[len(self.all_neuron_layers) - 1]
        self.delta_filter = []

        for i in range(0, len(self.all_filter)):
            delta_filter_helper: List[List[List[float]]] = []
            self.delta_filter.append(delta_filter_helper)
            for j in range(0, len(self.all_filter[i])):
                delta_filter_helper2: List[List[float]] = []
                self.delta_filter[i].append(delta_filter_helper2)
                for k in range(0, len(self.all_filter[i][j])):
                    delta_filter_helper3: List[float] = [0] * len(self.all_filter[i][j][k])
                    self.delta_filter[i][j].append(delta_filter_helper3)

        pooling_neurons: List[Neuron] = self.pooling_neurons
        for pooling in pooling_neurons:
            pooling.d_L_d_input = 0.0

        is_output_layer = True

        while True:
            neurons_to_process_next: Set[Neuron] = set()
            for neuron in neurons_to_process:
                if neuron not in self.input_synapses:
                    continue
                input_neurons: Dict[Neuron, float] = self.input_synapses[neuron]

                for input_neuron in input_neurons.keys():
                    input_neuron.output_to_train = 0.0
                    if isinstance(input_neuron, ActivationMapNeuron):
                        neurons_to_process_next.add(input_neuron)
                    if isinstance(input_neuron, PoolingNeuron):
                        neurons_to_process_next.add(input_neuron)

            for neuron in neurons_to_process:
                self._train(neuron.output_to_train, neuron, is_output_layer)

            is_output_layer = False
            neurons_to_process = neurons_to_process_next.copy()

            if len(neurons_to_process) <= 0:
                break

        self.update_weights(self.activation_map_neurons)
        return error

    def _train(self, expectedValue, neuron: Neuron, is_output_layer):
        if (is_output_layer):
            t_exp: float = math.exp(neuron.outputBeforeSoftmax)
            d_out_d_t: float = -self.t_exp_at_correct_class * t_exp / (self.sum_totals * self.sum_totals)
            if expectedValue != 0:
                d_out_d_t = self.t_exp_at_correct_class * (self.sum_totals - self.t_exp_at_correct_class) / \
                    (self.sum_totals * self.sum_totals)

            d_L_d_t: float = self.gradient * d_out_d_t

            if neuron not in self.input_synapses:
                return
            input_neurons: Dict[Neuron, float] = self.input_synapses[neuron]

            for input_neuron in input_neurons.keys():
                input_neuron.sumd_L_d_input(input_neurons.get(input_neuron) * d_L_d_t)
                d_L_d_w: float = input_neuron.get_output() * d_L_d_t
                newWeight: float = input_neurons.get(input_neuron) - (d_L_d_w * LAMBDA)
                self.add_or_change_synapse(input_neuron, neuron, newWeight)

        if isinstance(neuron, PoolingNeuron):
            if neuron not in self.input_synapses:
                return
            input_neurons: Dict[Neuron, float] = self.input_synapses[neuron]

            for input_neuron in input_neurons.keys():
                if input_neuron.get_bias_neuron() == 1:
                    input_neuron.d_L_d_input = neuron.d_L_d_input
                else:
                    input_neuron.d_L_d_input = 0.0

        if isinstance(neuron, ActivationMapNeuron):
            if neuron.imAZero == 0:
                neuron.d_L_d_input = 0.0
            input_neurons: Dict[Neuron, float] = self.input_synapses[neuron]
            counter: int = 0
            inputDepth: int = 0

            for input_neuron in input_neurons.keys():
                if counter >= len(self.delta_filter[neuron.Layer - 1][neuron.filterNum][inputDepth]):
                    inputDepth += 1
                    counter = 0
                self.delta_filter[neuron.Layer - 1][neuron.filterNum][inputDepth][
                    counter] += input_neuron.get_output() * neuron.d_L_d_input
                counter += 1
                if isinstance(input_neuron, PoolingNeuron):
                    input_neuron.sumd_L_d_input(input_neurons[input_neuron] * neuron.d_L_d_input)

    def update_weights(self, activation_map_neurons: List[List[List[Neuron]]]):
        for i in range(0, len(self.all_filter)):
            for j in range(0, len(self.all_filter[i])):
                for k in range(0, len(self.all_filter[i][j])):
                    for l in range(0, len(self.all_filter[i][j][k])):
                        self.all_filter[i][j][k][l] -= LAMBDA * self.delta_filter[i][j][k][l]

        for i in range(0, len(activation_map_neurons)):
            for j in range(0, len(activation_map_neurons[i])):
                for k in range(0, len(activation_map_neurons[i][j])):
                    input_neurons: Dict[Neuron, float] = self.input_synapses[activation_map_neurons[i][j][k]]
                    counter: int = 0
                    inputDepth: int = 0
                    for input_neuron in input_neurons.keys():
                        if counter >= len(self.all_filter[i][j][inputDepth]):
                            inputDepth += 1
                            counter = 0
                        self.add_or_change_synapse(input_neuron, activation_map_neurons[i][j][k],
                                                   self.all_filter[i][j][inputDepth][counter])
                        counter += 1

    def get_net_error(self, fine_log):
        error: float = 0.0
        outputAtCorrectClass: float = 0.0
        sum_of_squares: float = 0.0
        i: int = 0
        for output_neuron in self.output_neurons:
            sum_of_squares += math.exp(output_neuron.get_output())
        self.sum_totals = sum_of_squares
        label: List[float] = [0] * 10
        for output_neuron in self.output_neurons:
            output_neuron.outputBeforeSoftmax = output_neuron.get_output()
            output_neuron.output = math.exp(output_neuron.get_output()) / self.sum_totals

            if output_neuron.get_output_to_train() != 0:
                outputAtCorrectClass = output_neuron.get_output()
                self.t_exp_at_correct_class = math.exp(output_neuron.outputBeforeSoftmax)
                self.gradient = -1 / outputAtCorrectClass

            self.softmax_array[i] = output_neuron.get_output()
            # print(i)
            label[i] = output_neuron.get_output_to_train()
            i += 1

        if self.index_of_highest_value(self.softmax_array) == self.index_of_highest_value(label):
            self.acc += 1
        error = -math.log(outputAtCorrectClass)
        return error

    def index_of_highest_value(self, values: List[float]):
        index: int = 0
        for i in range(0, len(values)):
            if values[i] > values[index]:
                index = 1
        return index

    def add_all_filter(self, filter: List[List[List[float]]]):
        self.all_filter.append(filter)
