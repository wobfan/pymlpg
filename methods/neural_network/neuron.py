from typing import Dict

from methods.neural_network.neural_network import NeuralNetwork
from methods.neural_network.parameters import *

from abc import ABC, abstractmethod


class Neuron(ABC):
    def __init__(self, nn: NeuralNetwork, id: str):
        self.output: int = None
        self.fired: bool = False
        # Simulation run uses bias neurons as a normal input.
        # Implemented here for quicker referencing.
        self.bias_neuron = None
        self.output_to_train: float = None
        self.id: str = id
        self.nn: NeuralNetwork = nn

    def bias(self, bias=None):
        if self.nn is not None:
            import methods.neural_network.bias_neuron  # local import because otherwise circular dependency
            bias = RANDOM.uniform(-1.0, 1.0) if bias is None else bias
            self.bias_neuron = methods.neural_network.bias_neuron.BiasNeuron(self.nn, "bias:" + self.id)
            self.connect_input(self.bias_neuron, bias)
        return self

    def connect_input(self, input_neuron: 'Neuron', weight=None):
        weight = RANDOM.uniform(-1.0, 1.0) if weight is None else weight
        self.connect_and_or_change_weight(input_neuron, weight)

    def connect_and_or_change_weight(self, input_neuron: 'Neuron', weight):
        self.nn.add_or_change_synapse(input_neuron, self, self.restrict_value(weight))

    def restrict_value(self, value):
        return min(INPUT_TRUE, max(INPUT_FALSE, value))

    def get_bias(self):
        return self.nn.input_synapses[self][self.bias_neuron]

    @abstractmethod
    def train(self):
        pass

    @abstractmethod
    def undo_training(self):
        pass

    @abstractmethod
    def compute_output(self):
        pass

    def forward_propagation(self):
        self.output = self.compute_output()
        self.fired = self.output > (OUTPUT_TRUE - OUTPUT_FALSE) / 2

    def get_fired(self):
        return self.fired

    def str(self, firing_details):
        if firing_details:
            return self.id + "=" + ("U" if self.get_fired() is None else ("F" if self.get_fired() else "!F")) \
                + ":" + (str(self.output) if self.output is not None else "null")
        else:
            return str(self)

    def __str__(self):
        string = self.id
        string += "="
        input_neurons_2_weights: Dict[Neuron, float] = {}
        if self in self.nn.input_synapses:
            input_neurons_2_weights = self.nn.input_synapses[self]

            if len(input_neurons_2_weights) > 0:

                string += " ["

                for input in input_neurons_2_weights.keys():
                    string += input.id + ":" + str(input_neurons_2_weights[input]) + " "

                string += "]"
        return string

    def get_network(self):
        return self.nn

    def get_fired(self):
        return self.fired

    def get_bias_neuron(self):
        return self.bias_neuron

    def get_output(self):
        return self.output

    def get_id(self):
        return self.id

    def get_output_to_train(self):
        return self.output_to_train

    def set_fired(self, fired):
        self.fired = fired

    def set_output_to_train(self, output_to_train):
        self.output_to_train = output_to_train
