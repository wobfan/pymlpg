from __future__ import annotations

import typing
import logging

from abc import ABC, abstractmethod
from typing import List, Dict

from methods.neural_network.parameters import *

if typing.TYPE_CHECKING:
    from methods.neural_network.neuron import Neuron


class NeuralNetwork(ABC):
    def __init__(self):
        self.input_neurons: List[Neuron] = []
        self.hidden_neurons: List[Neuron] = []
        self.output_neurons: List[Neuron] = []
        self.hidden_output_neurons: List[Neuron] = []

        self.input_synapses: Dict[Neuron, Dict[Neuron, float]] = {}
        self.all_neuron_layers: List[List[Neuron]] = []
        self.size_input_dataset = 0

    def compute_layers(self):
        self.all_neuron_layers: List[List[Neuron]] = []
        self.all_neuron_layers.append(self.output_neurons)
        neurons_to_process = self.output_neurons.copy()

        while True:
            next_neuron_layer = []
            for neuron in neurons_to_process:
                if neuron in self.input_synapses:
                    input_neurons_2_weight = self.input_synapses[neuron]
                    next_neuron_layer.extend(input_neurons_2_weight.keys())
            if len(next_neuron_layer) <= 0:
                break

            next_neuron_layer = set(next_neuron_layer)  # Remove duplicates
            self.all_neuron_layers.insert(0, next_neuron_layer)
            neurons_to_process = next_neuron_layer.copy()

    def forward_propagation(self):
        for layer in self.all_neuron_layers:
            for neuron in layer:
                neuron.forward_propagation()
        return self

    def set_inputs(self, *inputs):
        number_inputs = len(inputs)
        if number_inputs != len(self.input_neurons):
            number_inputs = min(number_inputs, len(self.input_neurons))
            logging.critical("Number of input Neurons: " + str(len(self.input_neurons))
                             + "does not match the number of inputs requested to set: " + str(len(inputs))
                             + " setting to min( of both ): " + str(number_inputs))

        for n in range(0, number_inputs):
            input_neuron = self.input_neurons[n]
            input_neuron.change_input(inputs[n])

        return self

    def set_target_output(self, neuron: Neuron, output: float):
        neuron.output_to_train = output
        return self

    def set_target_outputs(self, *outputs):
        for n in range(0, len(outputs)):
            self.set_target_output(self.output_neurons[n], outputs[n])
        return self

    def get_net_error(self, fine_log):
        net_error = 0.0

        for output_neuron in self.output_neurons:
            net_error += self.get_error(output_neuron)

        if fine_log:
            fine_log.append("\t" + self.str(True) + ", Error: " + str(net_error) + "\n")

        return net_error

    def get_error(self, output_neuron: Neuron):
        return self.compute_half_squared_error(
            abs(OUTPUT_TRUE if output_neuron.fired else OUTPUT_FALSE) - output_neuron.output_to_train)

    @abstractmethod
    def train(self, error):
        pass

    def add_or_change_synapse(self, input_neuron: Neuron, output_neuron: Neuron, weight):
        if output_neuron in self.input_synapses:
            input_neurons_map = self.input_synapses[output_neuron]
        else:
            input_neurons_map: Dict[Neuron, int] = {}

        input_neurons_map[input_neuron] = weight
        self.input_synapses[output_neuron] = input_neurons_map

    def compute_half_squared_error(self, error: float):
        return error * error * 0.5

    def str(self, firing_details):
        string = "I{ "
        for neuron in self.input_neurons:
            string += neuron.str(firing_details) + " "
        string += "},  "

        if len(self.hidden_neurons) > 0:
            string += "H{ "
            for neuron in self.hidden_neurons:
                string += neuron.str(firing_details) + " "
            string += "},  "

        string += "O{ "
        for neuron in self.output_neurons:
            string += neuron.str(firing_details) + " "
        string += "}"

        return string

    def __str__(self):
        return self.str(False)
