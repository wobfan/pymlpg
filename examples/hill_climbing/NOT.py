import sys

from methods.neural_network.hill_climbing.neural_network_hill_climbing import NeuralNetworkHillClimbing
from methods.neural_network.hill_climbing.output_neuron_hill_climbing import OutputNeuronHillClimbing
from methods.util.fine_log import FineLog

sys.path.append("..")

from methods.neural_network.parameters import *
from methods.neural_network.input_neuron import InputNeuron

from methods.util.output_check import OutputCheck

nn = NeuralNetworkHillClimbing()
i1 = InputNeuron(nn, "I1")
o1 = OutputNeuronHillClimbing(nn, "O1").bias()

o1.connect_input(i1)
nn.compute_layers()

iteration = 0

while True:
    fine_log = FineLog()
    iteration += 1
    output_check = OutputCheck()

    sum_errors = (nn.set_inputs(INPUT_FALSE).forward_propagation().set_target_outputs(OUTPUT_TRUE)
                  .get_net_error(fine_log))
    output_check.add_computed_and_to_train(o1.get_fired(), False)

    sum_errors += (nn.set_inputs(INPUT_TRUE).forward_propagation().set_target_outputs(OUTPUT_FALSE)
                   .get_net_error(fine_log))
    output_check.add_computed_and_to_train(o1.get_fired(), True)

    print(str(iteration) + ". " + str(nn) + ", Error: " + str(sum_errors) + "\n" + str(fine_log))

    nn.train(sum_errors)

    if not (iteration < TRAININGCYCLES and output_check.all_non_equal_values() > 0):
        break
