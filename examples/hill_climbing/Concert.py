import sys

from methods.neural_network.hill_climbing.hidden_neuron_hill_climbing import HiddenNeuronHillClimbing
from methods.neural_network.hill_climbing.neural_network_hill_climbing import NeuralNetworkHillClimbing
from methods.neural_network.hill_climbing.output_neuron_hill_climbing import OutputNeuronHillClimbing

sys.path.append("..")

from methods.neural_network.input_neuron import InputNeuron
from methods.neural_network.parameters import *


from methods.util.output_check import OutputCheck

from methods.util.fine_log import FineLog

nn = NeuralNetworkHillClimbing()
n1 = InputNeuron(nn, "N1", INPUT_TRUE)
n2 = InputNeuron(nn, "N2", INPUT_TRUE)
n3 = InputNeuron(nn, "N3", INPUT_TRUE)

n4 = HiddenNeuronHillClimbing(nn, "N4").bias(0.3)
n5 = HiddenNeuronHillClimbing(nn, "N5").bias(0.3)

n6 = OutputNeuronHillClimbing(nn, "N6").bias(0.3)

n4.connect_input(n1, 0.3)
n4.connect_input(n2, 0.4)
n4.connect_input(n3, -0.7)

n5.connect_input(n1, -0.8)
n5.connect_input(n2, -0.5)
n5.connect_input(n3, 0.7)

n6.connect_input(n4, 0.4)
n6.connect_input(n5, 0.6)

nn.compute_layers()

iteration = 0

while True:
    fine_log = FineLog()
    iteration += 1
    output_check = OutputCheck()

    sum_errors = nn.set_inputs(INPUT_FALSE, INPUT_FALSE, INPUT_FALSE).forward_propagation().set_target_outputs(
        OUTPUT_FALSE).get_net_error(fine_log)
    output_check.add_computed_and_to_train(n6.get_fired(), False)
    sum_errors += nn.set_inputs(INPUT_FALSE, INPUT_FALSE, INPUT_TRUE).forward_propagation().set_target_outputs(
        OUTPUT_FALSE).get_net_error(fine_log)
    output_check.add_computed_and_to_train(n6.get_fired(), False)
    sum_errors += nn.set_inputs(INPUT_FALSE, INPUT_TRUE, INPUT_FALSE).forward_propagation().set_target_outputs(
        OUTPUT_FALSE).get_net_error(fine_log)
    output_check.add_computed_and_to_train(n6.get_fired(), False)
    sum_errors += nn.set_inputs(INPUT_TRUE, INPUT_FALSE, INPUT_FALSE).forward_propagation().set_target_outputs(
        OUTPUT_FALSE).get_net_error(fine_log)
    output_check.add_computed_and_to_train(n6.get_fired(), False)
    sum_errors += nn.set_inputs(INPUT_TRUE, INPUT_FALSE, INPUT_TRUE).forward_propagation().set_target_outputs(
        OUTPUT_FALSE).get_net_error(fine_log)
    output_check.add_computed_and_to_train(n6.get_fired(), False)
    sum_errors += nn.set_inputs(INPUT_TRUE, INPUT_TRUE, INPUT_FALSE).forward_propagation().set_target_outputs(
        OUTPUT_FALSE).get_net_error(fine_log)
    output_check.add_computed_and_to_train(n6.get_fired(), False)
    sum_errors += nn.set_inputs(INPUT_FALSE, INPUT_TRUE, INPUT_TRUE).forward_propagation().set_target_outputs(
        OUTPUT_FALSE).get_net_error(fine_log)
    output_check.add_computed_and_to_train(n6.get_fired(), False)
    sum_errors += nn.set_inputs(INPUT_TRUE, INPUT_TRUE, INPUT_TRUE).forward_propagation().set_target_outputs(
        OUTPUT_TRUE).get_net_error(fine_log)
    output_check.add_computed_and_to_train(n6.get_fired(), True)

    print(str(iteration) + ". " + str(nn) + ", Error: " + str(sum_errors) + "\n" + str(fine_log))

    nn.train(sum_errors)

    if not (iteration < TRAININGCYCLES and output_check.all_non_equal_values() > 0):
        break
