import sys

from methods.neural_network.hill_climbing.hidden_neuron_hill_climbing import HiddenNeuronHillClimbing
from methods.neural_network.hill_climbing.neural_network_hill_climbing import NeuralNetworkHillClimbing
from methods.neural_network.hill_climbing.output_neuron_hill_climbing import OutputNeuronHillClimbing
from methods.util.csv_import.CSVWriter import CSVWriter
from methods.util.fine_log import FineLog
from methods.util.scatter2D import scatter2D

sys.path.append("..")

from methods.neural_network.input_neuron import InputNeuron
from methods.neural_network.parameters import *
from methods.util.output_check import OutputCheck


def write_results_to_file(nn: HiddenNeuronHillClimbing, o1: OutputNeuronHillClimbing, filename):

    writer = CSVWriter(filename)

    # trained nn
    print("NN:False values written: " + writer.writeData(nn, o1, "NNFalse", False))
    print("NN:TRUE values written: " + writer.writeData(nn, o1, "NNTrue", True))

    # sample training
    writer.writeHeader("XORFalse", ["input1", "input2", "output"])
    writer.writeLine([str(INPUT_FALSE), str(INPUT_FALSE), str(OUTPUT_FALSE)])
    writer.writeLine([str(INPUT_TRUE), str(INPUT_TRUE), str(OUTPUT_FALSE)])
    writer.writeHeader("XORTrue", ["input1", "input2", "output"])
    writer.writeLine([str(INPUT_FALSE), str(INPUT_TRUE), str(OUTPUT_TRUE)])
    writer.writeLine([str(INPUT_TRUE, str(INPUT_TRUE), str(OUTPUT_TRUE))])

    writer.close()


nn = NeuralNetworkHillClimbing()
i1 = InputNeuron(nn, "I1")
i2 = InputNeuron(nn, "I2")
h1 = HiddenNeuronHillClimbing(nn, "H1").bias()
h2 = HiddenNeuronHillClimbing(nn, "H2").bias()
o1 = OutputNeuronHillClimbing(nn, "O1").bias()

h1.connect_input(i1)
h1.connect_input(i2)
h2.connect_input(i1)
h2.connect_input(i2)

o1.connect_input(h1)
o1.connect_input(h2)

nn.compute_layers()

iteration = 0
error = None

while True:
    fine_log = FineLog()
    output_check = OutputCheck()
    iteration += 1

    error = nn.train(
        nn.set_inputs(INPUT_FALSE, INPUT_FALSE).forward_propagation().set_target_outputs(OUTPUT_FALSE).get_net_error(
            fine_log))
    output_check.add_computed_and_to_train(o1.get_fired(), False)

    error += nn.train(
        nn.set_inputs(INPUT_TRUE, INPUT_FALSE).forward_propagation().set_target_outputs(OUTPUT_TRUE).get_net_error(
            fine_log))
    output_check.add_computed_and_to_train(o1.get_fired(), True)

    error += nn.train(
        nn.set_inputs(INPUT_FALSE, INPUT_TRUE).forward_propagation().set_target_outputs(OUTPUT_TRUE).get_net_error(
            fine_log))
    output_check.add_computed_and_to_train(o1.get_fired(), True)

    error += nn.train(
        nn.set_inputs(INPUT_TRUE, INPUT_TRUE).forward_propagation().set_target_outputs(INPUT_FALSE).get_net_error(
            fine_log))
    output_check.add_computed_and_to_train(o1.get_fired(), False)

    print(str(iteration) + ". " + str(nn) + ", Error: " + str(error) + "\n" + str(fine_log))

    if not (iteration < TRAININGCYCLES and (
            abs(error) >= ERROR_MARGIN_NET or output_check.all_non_equal_values() > 0)):
        break

write_results_to_file(nn, o1, "output_XOR.csv")
scatter2D("output_XOR.csv")
