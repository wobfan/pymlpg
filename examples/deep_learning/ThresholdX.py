import sys

from methods.util.csv_import.CSVWriter import CSVWriter
from methods.neural_network.input_neuron import InputNeuron
from methods.neural_network.parameters import *
from methods.neural_network.deep_learning.neural_network_deeplearning import NeuralNetworkDeepLearning
from methods.neural_network.deep_learning.output_neuron_deep_learning import OutputNeuronDeepLearning
from methods.neural_network.deep_learning.sigmoid import Sigmoid
from methods.util.labelled_point import LabelledPoint
from methods.util.labelled_points_and_boxes import LabelledPointsAndBoxes
from methods.util.output_check import OutputCheck
from typing import List

from methods.util.scatter2D import scatter2D

sys.path.append("../..")


def write_results_to_file(nn: NeuralNetworkDeepLearning, trainingPoints: LabelledPointsAndBoxes, o1: OutputNeuronDeepLearning, filename):

    writer = CSVWriter(filename)

    # trained nn
    print("NN:False values written: " + writer.writeData(nn, o1, "NNFalse", False))
    print("NN:TRUE values written: " + writer.writeData(nn, o1, "NNTrue", True))
    print("NN:UNKNOWN values written: " + writer.writeData(nn, o1, "NNUnknown", None))

    writer.writeHeader("THRESHOLDFalse", ["input1", "input2", "output"])

    for lp in trainingPoints.labelled_points:
        if lp.label is not None and lp.label == OUTPUT_FALSE:
            writer.writeLine([str(lp.x), str(lp.y), str(OUTPUT_FALSE)])

    writer.writeHeader("THRESHOLDFUnknown", ["input1", "input2", "output"])

    for lp in trainingPoints.labelled_points:
        if lp.label is None:
            writer.writeLine([str(lp.x), str(lp.y), str(OUTPUT_UNKNOWN)])

    # sample training
    writer.writeHeader("THRESHOLDTrue", ["input1", "input2", "output"])

    for lp in trainingPoints.labelled_points:
        if lp.label is not None and lp.label == OUTPUT_TRUE:
            writer.writeLine([str(lp.x), str(lp.y), str(OUTPUT_TRUE)])

    writer.close()


nn = NeuralNetworkDeepLearning(Sigmoid())

i1 = InputNeuron(nn, "I1")
i2 = InputNeuron(nn, "I2")
o1 = OutputNeuronDeepLearning(nn, "O1")
o2 = OutputNeuronDeepLearning(nn, "O2")

o1.connect_input(i1)
o1.connect_input(i2)

o2.connect_input(i1)
o2.connect_input(i2)

nn.compute_layers()
nn.size_input_dataset = 4

training_set: List[LabelledPointsAndBoxes] = []

training_points = LabelledPointsAndBoxes()
training_points.labelled_points.append(LabelledPoint(0.5, 0.0, o1, None))
training_points.labelled_points.append(LabelledPoint(0.5, 0.0, o2, OUTPUT_TRUE))
training_set.append(training_points)

training_points = LabelledPointsAndBoxes()
training_points.labelled_points.append(LabelledPoint(1.0, 0.0, o2, None))
training_points.labelled_points.append(LabelledPoint(1.0, 0.0, o1, OUTPUT_TRUE))
training_set.append(training_points)

iteration = 0

while True:
    fine_log = ""
    output_check = OutputCheck()
    net_error = 0.0
    iteration += 1

    for training_points in training_set:
        for lp in training_points.labelled_points:
            net_error += nn.set_inputs(lp.x, lp.y).forward_propagation().set_target_output(lp.neuron,
                                                                                           lp.label).get_error(
                lp.neuron)
            output_check.add_computed_and_to_train(lp.neuron.get_fired(),
                                                   None if lp.label is None else (lp.label == OUTPUT_TRUE))
        print(str(iteration) + ". " + str(nn) + ", Error: " + str(net_error))  # + "\n" + fine_log)
        nn.train(net_error)

    if not (iteration < TRAININGCYCLES and (
            abs(net_error) >= ERROR_MARGIN_NET or output_check.all_non_equal_values() > 0)):
        break

write_results_to_file(nn, training_points, o1, "output_THRESHOLDX.csv")
scatter2D("output_THRESHOLDX.csv")
