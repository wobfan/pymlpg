import sys

from methods.util.csv_import.CSVWriter import CSVWriter
from methods.util.fine_log import FineLog
from methods.neural_network.input_neuron import InputNeuron
from methods.neural_network.parameters import *
from methods.neural_network.deep_learning.neural_network_deeplearning import NeuralNetworkDeepLearning
from methods.neural_network.deep_learning.output_neuron_deep_learning import OutputNeuronDeepLearning
from methods.neural_network.deep_learning.sigmoid import Sigmoid
from methods.util.output_check import OutputCheck
from methods.util.scatter2D import scatter2D

sys.path.append("../..")


def write_results_to_file(nn: NeuralNetworkDeepLearning, o1: OutputNeuronDeepLearning, filename):
    writer = CSVWriter(filename)

    # trained nn
    print("NN:False values written: " + writer.writeData(nn, o1, "NNFalse", False))
    print("NN:TRUE values written: " + writer.writeData(nn, o1, "NNTrue", True))

    # sample training
    writer.writeHeader("ORFalse", ["input1", "input2", "output"])
    writer.writeLine([str(INPUT_FALSE), str(INPUT_FALSE), str(OUTPUT_FALSE)])
    writer.writeLine([str(INPUT_TRUE), str(INPUT_FALSE), str(OUTPUT_FALSE)])
    writer.writeLine([str(INPUT_FALSE), str(INPUT_TRUE), str(OUTPUT_FALSE)])
    writer.writeHeader("ORTrue", ["input1", "input2", "output"])
    writer.writeLine([str(INPUT_TRUE), str(INPUT_TRUE), str(OUTPUT_FALSE)])

    writer.close()


nn = NeuralNetworkDeepLearning(Sigmoid())
i1 = InputNeuron(nn, "I1")
i2 = InputNeuron(nn, "I2")
o1 = OutputNeuronDeepLearning(nn, "O1").bias()

o1.connect_input(i1)
o1.connect_input(i2)
nn.compute_layers()
nn.size_input_dataset = 4

iteration = 0
error = None

while True:
    fine_log = FineLog()
    output_check = OutputCheck()
    iteration += 1

    error = nn.train(
        nn.set_inputs(INPUT_FALSE, INPUT_FALSE).forward_propagation().set_target_outputs(OUTPUT_FALSE).get_net_error(
            fine_log))
    output_check.add_computed_and_to_train(o1.get_fired(), False)

    error += nn.train(
        nn.set_inputs(INPUT_TRUE, INPUT_FALSE).forward_propagation().set_target_outputs(OUTPUT_TRUE).get_net_error(
            fine_log))
    output_check.add_computed_and_to_train(o1.get_fired(), True)

    error += nn.train(
        nn.set_inputs(INPUT_FALSE, INPUT_TRUE).forward_propagation().set_target_outputs(OUTPUT_TRUE).get_net_error(
            fine_log))
    output_check.add_computed_and_to_train(o1.get_fired(), True)

    error += nn.train(
        nn.set_inputs(INPUT_TRUE, INPUT_TRUE).forward_propagation().set_target_outputs(OUTPUT_TRUE).get_net_error(
            fine_log))
    output_check.add_computed_and_to_train(o1.get_fired(), True)

    print(str(iteration) + ". " + str(nn) + ", Error: " + str(error) + "\n" + str(fine_log))

    if not (iteration < TRAININGCYCLES and (
            abs(error) >= ERROR_MARGIN_NET or output_check.all_non_equal_values() > 0)):
        break

write_results_to_file(nn, o1, "output_OR.csv")
scatter2D("output_OR.csv")
