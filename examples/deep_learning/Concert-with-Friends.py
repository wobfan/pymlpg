import sys

from methods.neural_network.input_neuron import InputNeuron
from methods.neural_network.parameters import *
from methods.neural_network.deep_learning.neural_network_deeplearning import NeuralNetworkDeepLearning
from methods.neural_network.deep_learning.sigmoid import Sigmoid
from methods.neural_network.deep_learning.hidden_neuron_deep_learning import HiddenNeuronDeepLearning
from methods.neural_network.deep_learning.output_neuron_deep_learning import OutputNeuronDeepLearning
from methods.util.csv_import.CSVWriter import CSVWriter
from methods.util.output_check import OutputCheck
from methods.util.fine_log import FineLog
from methods.util.scatter2D import scatter2D

sys.path.append("../..")


def write_results_to_file(nn: NeuralNetworkDeepLearning, o1: OutputNeuronDeepLearning, filename):
    writer = CSVWriter(filename)

    # sample training
    writer.writeHeader("CONCERTFalse", ["input1", "input2", "output"])
    writer.writeLine([str(INPUT_FALSE), str(INPUT_FALSE), str(INPUT_FALSE), str(OUTPUT_FALSE)])
    writer.writeLine([str(INPUT_FALSE), str(INPUT_FALSE), str(INPUT_TRUE), str(OUTPUT_FALSE)])
    writer.writeLine([str(INPUT_FALSE), str(INPUT_TRUE), str(INPUT_FALSE), str(OUTPUT_FALSE)])
    writer.writeLine([str(INPUT_TRUE), str(INPUT_FALSE), str(INPUT_FALSE), str(OUTPUT_FALSE)])
    writer.writeLine([str(INPUT_TRUE), str(INPUT_FALSE), str(INPUT_TRUE), str(OUTPUT_FALSE)])
    writer.writeLine([str(INPUT_FALSE), str(INPUT_TRUE), str(INPUT_TRUE), str(OUTPUT_FALSE)])
    writer.writeLine([str(INPUT_TRUE), str(INPUT_TRUE), str(INPUT_TRUE), str(OUTPUT_TRUE)])

    writer.writeHeader("CONCERTTrue", ["input1", "input2", "output"])
    writer.writeLine([str(INPUT_TRUE), str(INPUT_TRUE), str(OUTPUT_FALSE)])

    writer.close()


nn = NeuralNetworkDeepLearning(Sigmoid())
n1 = InputNeuron(nn, "N1", INPUT_TRUE)
n2 = InputNeuron(nn, "N2", INPUT_TRUE)
n3 = InputNeuron(nn, "N3", INPUT_TRUE)

n4 = HiddenNeuronDeepLearning(nn, "N4").bias(0.2)
n5 = HiddenNeuronDeepLearning(nn, "N5").bias(0.2)

n6 = OutputNeuronDeepLearning(nn, "N6").bias(0.3)

n4.connect_input(n1, 0.3)
n4.connect_input(n2, 0.4)
n4.connect_input(n3, -0.7)

n5.connect_input(n1, -0.8)
n5.connect_input(n2, -0.5)
n5.connect_input(n3, 0.7)

n6.connect_input(n4, 0.4)
n6.connect_input(n5, 0.6)

nn.compute_layers()
nn.size_input_dataset = 8

iteration = 0

while True:
    fine_log = FineLog()
    iteration += 1
    output_check = OutputCheck()

    sum_errors = nn.train(nn.set_inputs(INPUT_FALSE, INPUT_FALSE, INPUT_FALSE).forward_propagation().set_target_outputs(
        OUTPUT_FALSE).get_net_error(fine_log))
    output_check.add_computed_and_to_train(n6.get_fired(), False)
    sum_errors += nn.train(nn.set_inputs(INPUT_FALSE, INPUT_FALSE, INPUT_TRUE).forward_propagation().set_target_outputs(
        OUTPUT_TRUE).get_net_error(fine_log))
    output_check.add_computed_and_to_train(n6.get_fired(), True)
    sum_errors += nn.train(nn.set_inputs(INPUT_FALSE, INPUT_TRUE, INPUT_FALSE).forward_propagation().set_target_outputs(
        OUTPUT_FALSE).get_net_error(fine_log))
    output_check.add_computed_and_to_train(n6.get_fired(), False)
    sum_errors += nn.train(nn.set_inputs(INPUT_TRUE, INPUT_FALSE, INPUT_FALSE).forward_propagation().set_target_outputs(
        OUTPUT_FALSE).get_net_error(fine_log))
    output_check.add_computed_and_to_train(n6.get_fired(), False)
    sum_errors += nn.train(nn.set_inputs(INPUT_TRUE, INPUT_FALSE, INPUT_TRUE).forward_propagation().set_target_outputs(
        OUTPUT_TRUE).get_net_error(fine_log))
    output_check.add_computed_and_to_train(n6.get_fired(), True)
    sum_errors += nn.train(nn.set_inputs(INPUT_TRUE, INPUT_TRUE, INPUT_FALSE).forward_propagation().set_target_outputs(
        OUTPUT_FALSE).get_net_error(fine_log))
    output_check.add_computed_and_to_train(n6.get_fired(), False)
    sum_errors += nn.train(nn.set_inputs(INPUT_FALSE, INPUT_TRUE, INPUT_TRUE).forward_propagation().set_target_outputs(
        OUTPUT_TRUE).get_net_error(fine_log))
    output_check.add_computed_and_to_train(n6.get_fired(), True)
    sum_errors += nn.train(nn.set_inputs(INPUT_TRUE, INPUT_TRUE, INPUT_TRUE).forward_propagation().set_target_outputs(
        OUTPUT_TRUE).get_net_error(fine_log))
    output_check.add_computed_and_to_train(n6.get_fired(), True)

    print(str(iteration) + ". " + str(nn) + ", Error: " + str(sum_errors) + "\n" + str(fine_log))

    if not (iteration < TRAININGCYCLES and (
            abs(sum_errors) >= ERROR_MARGIN_NET or output_check.all_non_equal_values() > 0)):
        break

write_results_to_file(nn, n6, "output_CONCERT.csv")
scatter2D("output_CONCERT.csv")
