import sys

from methods.util.fine_log import FineLog
from methods.neural_network.input_neuron import InputNeuron
from methods.neural_network.parameters import *
from methods.neural_network.deep_learning.neural_network_deeplearning import NeuralNetworkDeepLearning
from methods.neural_network.deep_learning.hidden_neuron_deep_learning import HiddenNeuronDeepLearning
from methods.neural_network.deep_learning.output_neuron_deep_learning import OutputNeuronDeepLearning
from methods.neural_network.deep_learning.sigmoid import Sigmoid
from methods.util.box import Box
from methods.util.labelled_points_and_boxes import LabelledPointsAndBoxes
from methods.util.csv_import.CSVWriter import CSVWriter
from methods.util.scatter2D import scatter2D

sys.path.append("../..")


def write_results_to_file(nn: NeuralNetworkDeepLearning, trainingPoints: LabelledPointsAndBoxes, o1: OutputNeuronDeepLearning, filename):

    writer = CSVWriter(filename)

    # trained nn
    print("NN:False values written: " + writer.writeData(nn, o1, "NNFalse", False))
    print("NN:TRUE values written: " + writer.writeData(nn, o1, "NNTrue", True))

    # sample training
    writer.writeHeader("BOXFalse", ["input1", "input2", "output"])

    for lp in trainingPoints.labelled_points:
        if lp.label == OUTPUT_FALSE:
            writer.writeLine([str(lp.x), str(lp.y), str(OUTPUT_FALSE)])

    # sample training
    writer.writeHeader("BOXTrue", ["input1", "input2", "output"])

    for lp in trainingPoints.labelled_points:
        if lp.label == OUTPUT_TRUE:
            writer.writeLine([str(lp.x), str(lp.y), str(OUTPUT_TRUE)])

    writer.close()


nn = NeuralNetworkDeepLearning(Sigmoid())

i1 = InputNeuron(nn, "I1")
i2 = InputNeuron(nn, "I2")

h1 = HiddenNeuronDeepLearning(nn, "H1").bias()
h1.connect_input(i1)
h2 = HiddenNeuronDeepLearning(nn, "H2").bias()
h2.connect_input(i1)
h3 = HiddenNeuronDeepLearning(nn, "H3").bias()
h3.connect_input(i2)
h4 = HiddenNeuronDeepLearning(nn, "H4").bias()
h4.connect_input(i2)

h5 = HiddenNeuronDeepLearning(nn, "H5").bias()
h5.connect_input(h1)
h5.connect_input(h2)
h6 = HiddenNeuronDeepLearning(nn, "H6").bias()
h6.connect_input(h3)
h6.connect_input(h4)

o1 = OutputNeuronDeepLearning(nn, "O1").bias()
o1.connect_input(h5)
o1.connect_input(h6)

nn.compute_layers()
nn.size_input_dataset = 400

LabelledPointsAndBoxes.boxes.append(Box(0.25, 0.25, 0.75, 0.75))
training_points = LabelledPointsAndBoxes(400, o1, True)

iteration = 0

while True:
    fine_log = FineLog()
    net_error = 0.0
    for lp in training_points.labelled_points:
        net_error += nn.set_inputs(lp.x, lp.y).forward_propagation().set_target_outputs(lp.label).get_net_error(None)

    print(str(iteration) + ". " + str(nn) + ", Error: " + str(net_error))  # + "\n" + str(fine_log))
    nn.train(net_error)

    iteration += 1

    if not (iteration < TRAININGCYCLES and (abs(net_error) >= ERROR_MARGIN_NET)):
        break

write_results_to_file(nn, training_points, o1, "output_XYBox.csv")
scatter2D("C://Users//Marcel//PycharmProjects//pymlpg//examples//deep_learning//output_XYBox.csv")
