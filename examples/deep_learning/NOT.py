import sys

from methods.util.fine_log import FineLog
from methods.neural_network.parameters import *
from methods.neural_network.input_neuron import InputNeuron
from methods.neural_network.deep_learning.neural_network_deeplearning import NeuralNetworkDeepLearning
from methods.neural_network.deep_learning.output_neuron_deep_learning import OutputNeuronDeepLearning
from methods.neural_network.deep_learning.sigmoid import Sigmoid
from methods.util.output_check import OutputCheck

sys.path.append("../..")

nn = NeuralNetworkDeepLearning(Sigmoid())
i1 = InputNeuron(nn, "I1")
o1 = OutputNeuronDeepLearning(nn, "O1").bias()

o1.connect_input(i1)
nn.compute_layers()
nn.size_input_dataset = 2

iteration = 0

while True:
    fine_log = FineLog()
    iteration += 1
    output_check = OutputCheck()

    sum_errors = nn.train(
        nn.set_inputs(INPUT_FALSE).forward_propagation().set_target_outputs(OUTPUT_TRUE).get_net_error(fine_log))
    output_check.add_computed_and_to_train(o1.get_fired(), True)

    sum_errors += nn.train(
        nn.set_inputs(INPUT_TRUE).forward_propagation().set_target_outputs(OUTPUT_FALSE).get_net_error(fine_log))
    output_check.add_computed_and_to_train(o1.get_fired(), False)

    print(str(iteration) + ". " + str(nn) + ", Error: " + str(sum_errors) + "\n" + str(fine_log))

    if not (iteration < TRAININGCYCLES and (
            abs(sum_errors) >= ERROR_MARGIN_NET or output_check.all_non_equal_values() > 0)):
        break
