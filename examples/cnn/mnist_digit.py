from __future__ import annotations

import math
import struct
import typing
from typing import List

from methods.neural_network.cnn.activation_map_neuron import ActivationMapNeuron
from methods.neural_network.cnn.neural_network_convolution import NeuralNetworkConvolution
from methods.neural_network.cnn.pooling_neuron import PoolingNeuron
from methods.neural_network.input_neuron import InputNeuron
from methods.neural_network.neuron import Neuron
from methods.neural_network.cnn.output_neuron_convolution import OutputNeuronConvolution
from methods.neural_network.cnn.regions_helper import RegionsHelper
from methods.neural_network.cnn.padding_neurons import PaddingNeurons
from methods.neural_network.cnn.train_set import TrainSet

if typing.TYPE_CHECKING:
    from methods.neural_network.neural_network import NeuralNetwork
    from methods.neural_network.parameters import *


def trainData(cnn: NeuralNetworkConvolution, set: TrainSet, loops: int):
    fineLog = ''
    for i in range(0, loops):
        for j in range(0, set.getDataSize()):
            cnn.set_inputs(*set.getInput(j))
            cnn.forward_propagation()
            cnn.set_target_outputs(*set.getOutput(j))
            error = cnn.get_net_error(fineLog)
            cnn.train(error)
            if j % 100 == 0:
                print("Step: " + str(j) + " Accuracy: " + str(cnn.acc))
                cnn.acc = 0
            if j % 300 == 0:
                cnn.set_inputs(*set.getInput(j))
                cnn.forward_propagation()
                out = set.getOutput(j)


def createTrainSet(start: int, end: int):
    set: TrainSet = TrainSet(28 * 28, 10)

    m = open("../data/mnist/digit/train-images.idx3-ubyte", "rb")
    l = open("../data/mnist/digit/train-labels.idx1-ubyte", "rb")

    magic_nr_label, size_label = struct.unpack(">II", l.read(8))
    magic_nr_images, size_images, rows_images, cols_images = struct.unpack(">IIII", m.read(16))

    for i_data_set in range(start, end):
        label: List[float] = [0] * 10
        label[int(struct.unpack(">B", l.read(1))[0])] = 1

        image: List = []
        for p in range(28 * 28):
            image.append(struct.unpack(">B", m.read(1))[0] / 256 - 0.5)

        set.addData(image, label)

    m.close()
    l.close()

    return set


def createInputNeurons(inputLength: int, cnn: NeuralNetworkConvolution):
    inputNeurons: List[Neuron] = []
    for i in range(0, inputLength):
        inputNeuron: Neuron = InputNeuron(cnn, "InputNeuron: " + str(i))
        inputNeurons.append(inputNeuron)

    return inputNeurons


def createConvolutionalLayer(inputHeight: int, inputWidth: int, inputDepth: int, numOfFilters: int, filterHeight: int,
                             filterWidth: int, convolutionalLayer: int, cnn: NeuralNetworkConvolution):
    newHeight: int = inputHeight - filterHeight + 2 + 1
    newWidth: int = inputWidth - filterWidth + 2 + 1
    numOfNeurons: int = newHeight * newWidth

    allActivationMaps: List[List[Neuron]] = []
    for i in range(0, numOfFilters):
        activationMap: List[Neuron] = []
        for j in range(0, numOfNeurons):
            activationMapNeuron: Neuron = ActivationMapNeuron(cnn, "AM" + str(i) + ": " + str(j), None, i, j,
                                                              convolutionalLayer, inputDepth)
            activationMap.append(activationMapNeuron)
        allActivationMaps.append(activationMap)

    random.seed(1)
    allFilter: List[List[List[float]]] = []
    for i in range(0, numOfFilters):
        newFilter: List[List[float]] = []
        for j in range(0, inputDepth):
            filter: List[float] = [None] * filterHeight * filterWidth
            for k in range(0, filterHeight * filterWidth):
                filter[k] = random.gauss(0, 1) * math.sqrt(2.0 / (filterHeight * filterWidth * inputDepth))
            newFilter.append(filter)
        allFilter.append(newFilter)
    cnn.add_all_filter(allFilter)
    return allActivationMaps


def createMaxPoolingLayer(inputHeight: int, inputWidth: int, numOfFiltersPreviousLayer: int, poolingLayer: int,
                          cnn: NeuralNetworkConvolution, inputDepth: int):
    newHeight: int = inputHeight // 2
    newWidth: int = inputWidth // 2
    numOfNeurons: int = newHeight * newWidth
    allPoolingNeurons: List[List[Neuron]] = []
    for i in range(0, numOfFiltersPreviousLayer):
        poolingNeurons: List[Neuron] = []
        for j in range(0, numOfNeurons):
            poolingNeuron: Neuron = PoolingNeuron(cnn, "P" + str(i) + ": " + str(j), None, i, j, poolingLayer,
                                                  inputDepth)
            poolingNeurons.append(poolingNeuron)
        allPoolingNeurons.append(poolingNeurons)
    return allPoolingNeurons


def createOutputNeurons(cnn: NeuralNetworkConvolution, numOfOutputNeurons: int):
    outputNeurons: List[Neuron] = []

    for i in range(0, numOfOutputNeurons):
        outputNeuron: Neuron = OutputNeuronConvolution(cnn, "O:" + str(i), None, 111, i, 111, 1111)
        outputNeurons.append(outputNeuron)
    return outputNeurons


def connectInput2Conv(allFilters: List[List[List[List[float]]]], allActivationMapNeuronsLayer1: List[List[Neuron]],
                      inputNeurons: List[Neuron], filterDepth: int, filterHeight: int, filterWidth: int,
                      cnn: NeuralNetwork):
    convRegion: RegionsHelper = RegionsHelper()
    localConForActivationMapNeurons: List[List[int][int]] = convRegion.createRegionsConvolutionalLayer(28, 28,
                                                                                                       filterHeight,
                                                                                                       filterWidth, 1)
    convolutionalLayer: int = 1

    # print(localConForActivationMapNeurons)
    if len(allFilters[convolutionalLayer - 1]) == len(allActivationMapNeuronsLayer1):
        for i in range(0, len(allFilters[convolutionalLayer - 1])):
            activationMapNeurons: List[Neuron] = allActivationMapNeuronsLayer1[i]
            counter: int = 0
            filter: List[float] = allFilters[convolutionalLayer - 1][i][filterDepth - 1]
            for j in range(len(allActivationMapNeuronsLayer1[i])):
                for k in range(filterHeight):
                    for l in range(filterWidth):
                        if localConForActivationMapNeurons[j][k][l] == -1:
                            paddingNeuron: Neuron = PaddingNeurons(cnn, "PN: 1", None)
                            activationMapNeurons[j].connect_input(paddingNeuron, filter[counter])
                        else:
                            activationMapNeurons[j].connect_input(
                                inputNeurons[localConForActivationMapNeurons[j][k][l]], filter[counter])
                        if counter == filterHeight * filterWidth - 1:
                            counter = 0
                        else:
                            counter += 1


def connectConv2Pool(allActivationMapNeurons: List[List[Neuron]], allPoolingNeurons: List[List[Neuron]], oldHeight: int,
                     oldWidth: int, windowHeight: int, windowWidth: int):
    poolingRegion: RegionsHelper = RegionsHelper()
    localConForPoolingNeurons: List[List[int][int]] = poolingRegion.createRegionsPoolingLayer(oldHeight, oldWidth)

    poolingNeurons2connect: List[Neuron] = []
    activationMapNeurons2connect: List[Neuron] = []

    if len(allActivationMapNeurons) == len(allPoolingNeurons):
        for i in range(0, len(allPoolingNeurons)):
            poolingNeurons2connect = allPoolingNeurons[i]
            activationMapNeurons2connect = allActivationMapNeurons[i]
            for j in range(0, len(poolingNeurons2connect)):
                for k in range(0, windowHeight):
                    for l in range(0, windowWidth):
                        poolingNeurons2connect[j].connect_input(
                            activationMapNeurons2connect[localConForPoolingNeurons[j][k][l]], 0.0)
    else:
        print("Fehler")


def connectPool2Conv(filter: List[List[List[float]]], allActivationMapNeurons: List[List[Neuron]],
                     allPoolingNeurons: List[List[Neuron]], oldHeight: int, oldWidth: int, filterHeight: int,
                     filterWidth: int, cnn: NeuralNetwork):
    convRegion: RegionsHelper = RegionsHelper()
    localConForActivationMapNeurons: List[List[int][int]] = convRegion.createRegionsConvolutionalLayer(oldHeight,
                                                                                                       oldWidth,
                                                                                                       filterHeight,
                                                                                                       filterWidth, 1)

    poolingNeurons2connect: List[Neuron] = []
    activationMapNeurons2connect: List[Neuron] = []

    for i in range(0, len(allActivationMapNeurons)):
        activationMapNeurons2connect = allActivationMapNeurons[i]
        for j in range(0, len(allPoolingNeurons)):
            poolingNeurons2connect = allPoolingNeurons[j]
            counter: int = 0
            for k in range(0, len(activationMapNeurons2connect)):
                for l in range(0, filterHeight):
                    for o in range(0, filterWidth):
                        if localConForActivationMapNeurons[k][l][o] == -1:
                            paddingNeuron: Neuron = PaddingNeurons(cnn, "PN: 1", None)
                            activationMapNeurons2connect[k].connect_input(paddingNeuron, filter[i][j][counter])
                        else:
                            activationMapNeurons2connect[k].connect_input(
                                poolingNeurons2connect[localConForActivationMapNeurons[k][l][o]], filter[i][j][counter])

                        if counter == filterHeight * filterWidth - 1:
                            counter = 0
                        else:
                            counter += 1


def connectPool2Output(allPoolingNeurons: List[List[Neuron]], allOutputNeurons: List[Neuron]):
    for outputNeuron in allOutputNeurons:
        for i in range(0, len(allPoolingNeurons)):
            for j in range(0, len(allPoolingNeurons[i])):
                outputNeuron.connect_input(allPoolingNeurons[i][j], 192)


def padArray(arr: List[int][int], padWidth: int, numOfPads: int):
    temp: List[int][int] = []
    for i in range(0, len(temp)):
        for j in range(0, len(temp[i])):
            temp[i][j] = padWidth
    for i in range(0, len(arr)):
        for j in range(0, len(arr[i])):
            temp[i + numOfPads][j + numOfPads] = arr[i][j]
    return temp


inputNeurons: List[Neuron]
allActivationMapNeuronsConvLayer1: List[List[Neuron]]
allPoolingNeuronsPoolingLayer1: List[List[Neuron]]
allActivationMapNeuronsConvLayer2: List[List[Neuron]]
allPoolingNeuronsPoolingLayer2: List[List[Neuron]]
allOutputNeurons: List[Neuron]

cnn: NeuralNetworkConvolution = NeuralNetworkConvolution(2)

inputNeurons = createInputNeurons(28 * 28, cnn)
allActivationMapNeuronsConvLayer1 = createConvolutionalLayer(28, 28, 1, 8, 3, 3, 1, cnn)
cnn.activation_map_neurons.append(allActivationMapNeuronsConvLayer1)
allPoolingNeuronsPoolingLayer1 = createMaxPoolingLayer(28, 28, 8, 1, cnn, 8)
allActivationMapNeuronsConvLayer2 = createConvolutionalLayer(14, 14, 12, 16, 3, 3, 2, cnn)
cnn.activation_map_neurons.append(allActivationMapNeuronsConvLayer2)
allPoolingNeuronsPoolingLayer2 = createMaxPoolingLayer(14, 14, 16, 2, cnn, 16)
allOutputNeurons = createOutputNeurons(cnn, 10)

connectInput2Conv(cnn.all_filter, allActivationMapNeuronsConvLayer1, inputNeurons, 1, 3, 3, cnn)
connectConv2Pool(allActivationMapNeuronsConvLayer1, allPoolingNeuronsPoolingLayer1, 28, 28, 2, 2)
connectPool2Conv(cnn.all_filter[1], allActivationMapNeuronsConvLayer2, allPoolingNeuronsPoolingLayer1, 14, 14, 3, 3,
                 cnn)
connectConv2Pool(allActivationMapNeuronsConvLayer2, allPoolingNeuronsPoolingLayer2, 14, 14, 2, 2)
connectPool2Output(allPoolingNeuronsPoolingLayer2, allOutputNeurons)

cnn.compute_layers()
cnn.size_input_dataset = 1000
trainSet: TrainSet = createTrainSet(0, 1000)
trainData(cnn, trainSet, 3)
