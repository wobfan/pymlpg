import sys

sys.path.append("../../..")

from methods.neural_network.deep_learning.sigmoid import Sigmoid
from methods.neural_network.deep_learning.tanh import Tanh
from methods.neural_network.deep_learning.relu import RELU

import unittest


class ActivationFunctionTest(unittest.TestCase):

    def test_computeSigmoid(self):
        activationFunction = Sigmoid()
        value = activationFunction.compute(0.5)

        self.assertAlmostEqual(0.6224593312018546, value)

    def test_computeTanH(self):
        activationFunction = Tanh()
        value = activationFunction.compute(0.5)

        self.assertAlmostEqual(0.46211715726000974, value)

    def test_computeRELUBigger0(self):
        activationFunction = RELU()
        value = activationFunction.compute(0.5)

        self.assertAlmostEqual(0.5, value)

    def test_computeRELULess0(self):
        activationFunction = RELU()
        value = activationFunction.compute(0.5)

        self.assertAlmostEqual(0.5, value)

    def test_computeDerivationOfNodeSigmoid(self):
        activationFunction = Sigmoid()
        value = activationFunction.compute_derivation_of_node(0.5)

        self.assertAlmostEqual(0.25, value)

    def test_computeDerivationOfNodeTanH(self):
        activationFunction = Tanh()
        value = activationFunction.compute_derivation_of_node(0.5)

        self.assertAlmostEqual(0.7864477329659274, value)

    def test_computeDerivationOfNodeRELUBigger0(self):
        activationFunction = RELU()
        value = activationFunction.compute_derivation_of_node(0.5)

        self.assertEqual(1, value)

    def test_computeDerivationOfNodeRELULess0(self):
        activationFunction = RELU()
        value = activationFunction.compute_derivation_of_node(-0.5)

        self.assertEqual(0, value)

    def test_computeOutputLayerSigmoid(self):
        activationFunction = Sigmoid()
        value = activationFunction.compute_delta_output_layer(0.3, 0.7, 0.8)

        self.assertAlmostEqual(0.06720000000000001, value)

    def test_computeOutputLayerTanH(self):
        activationFunction = Tanh()
        value = activationFunction.compute_delta_output_layer(0.3, 0.7, 0.8)

        self.assertAlmostEqual(0.20311666879438675, value)

    def test_computeOutputLayerRELU(self):
        activationFunction = RELU()
        value = activationFunction.compute_delta_output_layer(0.3, 0.7, 0.8)

        self.assertAlmostEqual(0.32, value)

    def test_computeHiddenLayerSigmoid(self):
        activationFunction = Sigmoid()
        value = activationFunction.compute_delta_hidden_layer(0.3, 0.7, 0.8)

        self.assertEqual(0.0504, value)

    def test_computeHiddenLayerTanH(self):
        activationFunction = Tanh()
        value = activationFunction.compute_delta_hidden_layer(0.3, 0.7, 0.8)

        self.assertAlmostEqual(0.15233750159579007, value)

    def test_computeHiddenLayerRELU(self):
        activationFunction = RELU()
        value = activationFunction.compute_delta_hidden_layer(0.3, 0.7, 0.8)

        self.assertEqual(0.24, value)


if __name__ == '__main__':
    unittest.main()
