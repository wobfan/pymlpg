import sys

from methods.neural_network.deep_learning.hidden_neuron_deep_learning import HiddenNeuronDeepLearning
from methods.neural_network.deep_learning.neural_network_deeplearning import NeuralNetworkDeepLearning
from methods.neural_network.deep_learning.output_neuron_deep_learning import OutputNeuronDeepLearning
from methods.neural_network.input_neuron import InputNeuron

sys.path.append("../../..")

from methods.neural_network.deep_learning.sigmoid import Sigmoid
from methods.neural_network.deep_learning.tanh import Tanh
from methods.neural_network.deep_learning.relu import RELU

import unittest


class BackpropagationTest(unittest.TestCase):

    def setUp(self):
        nn = NeuralNetworkDeepLearning(Sigmoid())
        self.nn = nn
        self.i1 = InputNeuron(nn, "I1")
        self.i2 = InputNeuron(nn, "I2")
        self.h1 = HiddenNeuronDeepLearning(nn, "H1").bias(0.35)
        self.h11 = HiddenNeuronDeepLearning(nn, "H11").bias()
        self.h2 = HiddenNeuronDeepLearning(nn, "H2").bias(0.35)
        self.h3 = HiddenNeuronDeepLearning(nn, "H3").bias(0.35)
        self.h4 = HiddenNeuronDeepLearning(nn, "H4").bias(0.35)
        self.o1 = OutputNeuronDeepLearning(nn, "O1").bias(0.6)
        self.o2 = OutputNeuronDeepLearning(nn, "O2").bias(0.6)

        nnTanh = NeuralNetworkDeepLearning(Tanh())
        self.nnTanh = nnTanh
        self.i1Tanh = InputNeuron(nnTanh, "I1")
        self.i2Tanh = InputNeuron(nnTanh, "I2")
        self.h1Tanh = HiddenNeuronDeepLearning(nnTanh, "H1").bias(0.35)
        self.h2Tanh = HiddenNeuronDeepLearning(nnTanh, "H2").bias(0.35)
        self.o1Tanh = OutputNeuronDeepLearning(nnTanh, "O1").bias(0.6)
        self.o2Tanh = OutputNeuronDeepLearning(nnTanh, "O2").bias(0.6)

        nnRELU = NeuralNetworkDeepLearning(RELU())
        self.nnRELU = nnRELU
        self.i1RELU = InputNeuron(nnRELU, "I1")
        self.i2RELU = InputNeuron(nnRELU, "I2")
        self.h1RELU = HiddenNeuronDeepLearning(nnRELU, "H1").bias(0.35)
        self.h2RELU = HiddenNeuronDeepLearning(nnRELU, "H2").bias(0.35)
        self.o1RELU = OutputNeuronDeepLearning(nnRELU, "O1").bias(0.6)
        self.o2RELU = OutputNeuronDeepLearning(nnRELU, "O2").bias(0.6)
        self.error = 0.0
        self.errorTanh = 0.0
        self.errorRELU = 0.0

    def setup_sigmoid(self):
        self.h1.connect_input(self.i1, 0.15)
        self.h1.connect_input(self.i2, 0.2)

        self.h2.connect_input(self.i1, 0.25)
        self.h2.connect_input(self.i2, 0.3)

        self.o1.connect_input(self.h1, 0.4)
        self.o1.connect_input(self.h2, 0.45)

        self.o2.connect_input(self.h1, 0.5)
        self.o2.connect_input(self.h2, 0.55)

        self.nn.compute_layers()
        self.nn.size_input_dataset = 1
        self.error = self.nn.set_inputs(0.05, 0.1).set_target_outputs(0.01, 0.99) \
            .forward_propagation().get_net_error(None)

    def setup_tanh(self):
        self.h1Tanh.connect_input(self.i1Tanh, 0.15)
        self.h1Tanh.connect_input(self.i2Tanh, 0.2)

        self.h2Tanh.connect_input(self.i1Tanh, 0.25)
        self.h2Tanh.connect_input(self.i2Tanh, 0.3)

        self.o1Tanh.connect_input(self.h1Tanh, 0.4)
        self.o1Tanh.connect_input(self.h2Tanh, 0.45)

        self.o2Tanh.connect_input(self.h1Tanh, 0.5)
        self.o2Tanh.connect_input(self.h2Tanh, 0.55)

        self.nnTanh.compute_layers()
        self.nnTanh.size_input_dataset = 1
        self.error = self.nnTanh.set_inputs(0.05, 0.1).set_target_outputs(0.01, 0.99) \
            .forward_propagation().get_net_error(None)

    def setup_RELU(self):
        self.h1RELU.connect_input(self.i1RELU, 0.15)
        self.h1RELU.connect_input(self.i2RELU, 0.2)

        self.h2RELU.connect_input(self.i1RELU, 0.25)
        self.h2RELU.connect_input(self.i2RELU, 0.3)

        self.o1RELU.connect_input(self.h1RELU, 0.4)
        self.o1RELU.connect_input(self.h2RELU, 0.45)

        self.o2RELU.connect_input(self.h1RELU, 0.5)
        self.o2RELU.connect_input(self.h2RELU, 0.55)

        self.nnRELU.compute_layers()
        self.nnRELU.size_input_dataset = 1
        self.error = self.nnRELU.set_inputs(0.05, 0.1).set_target_outputs(0.01, 0.99) \
            .forward_propagation().get_net_error(None)

    def setup_random_hidden_bias(self):
        self.h11.connect_input(self.i1, 0.15)
        self.h11.connect_input(self.i2, 0.2)

        self.h2.connect_input(self.i1, 0.25)
        self.h2.connect_input(self.i2, 0.3)

        self.o1.connect_input(self.h11, 0.4)
        self.o1.connect_input(self.h2, 0.45)

        self.o2.connect_input(self.h11, 0.5)
        self.o2.connect_input(self.h2, 0.55)

        self.nn.compute_layers()
        self.nn.size_input_dataset = 1
        self.error = self.nn.set_inputs(0.05, 0.1).set_target_outputs(0.01, 0.99) \
            .forward_propagation().get_net_error(None)

    def setup_two_hidden_layers(self):
        self.h1.connect_input(self.i1, 0.15)
        self.h1.connect_input(self.i2, 0.2)

        self.h2.connect_input(self.i1, 0.25)
        self.h2.connect_input(self.i2, 0.3)

        self.h3.connect_input(self.h1, 0.15)
        self.h3.connect_input(self.h2, 0.2)

        self.h4.connect_input(self.h1, 0.25)
        self.h4.connect_input(self.h2, 0.3)

        self.o1.connect_input(self.h3, 0.4)
        self.o1.connect_input(self.h4, 0.45)

        self.o2.connect_input(self.h3, 0.5)
        self.o2.connect_input(self.h4, 0.55)

        self.nn.compute_layers()
        self.nn.size_input_dataset = 1
        self.error = self.nn.set_inputs(0.05, 0.1).set_target_outputs(0.01, 0.99) \
            .forward_propagation().get_net_error(None)

    def test_forward_propagation_initial_sigmoid(self):
        self.setup_sigmoid()
        self.assertEqual(0.2983711087600027, self.error, "Initial Network error incorrect (sigmoid).")

    def test_forward_propagation_initial_tanh(self):
        self.setup_tanh()
        self.assertEqual(0.28112693346345974, self.error, "Initial Network error incorrect (tanh).")

    def test_forward_propagation_initial_RELU(self):
        self.setup_RELU()
        self.assertEqual(0.4211247656249999, self.error, "Initial Network error incorrect (relu).")

    def test_forward_propagation_initial_two_hidden_layers(self):
        self.setup_two_hidden_layers()
        self.assertEqual(0.3027140236299515, self.error, "Initial Network error incorrect (two hidden).")

    def test_forward_propagation_initial_random_hidden_bias(self):
        self.setup_random_hidden_bias()
        self.assertEqual(0.2915958637723763, self.error, "Initial Network error incorrect (random bias).")

    def test_get_error_fired_true(self):
        self.setup_sigmoid()
        self.o1.fired = True
        self.o1.output_to_train = None
        self.assertEqual(0.5291825288575182, self.nn.get_error(self.o1))

    def test_get_error_fired_false(self):
        self.setup_sigmoid()
        self.o1.fired = False
        self.o1.output_to_train = None
        self.assertEqual(1.0264523897528868, self.nn.get_error(self.o1))

    def test_backpropagation_initial_two_hidden_layers(self):
        self.setup_two_hidden_layers()
        self.nn.train(self.error)

        self.assertEqual(0.2989968793888064, self.nn.forward_propagation().get_net_error(None),
                         "Network error after single training incorrect.")

    def test_backpropagation_initial_tanh(self):
        self.setup_tanh()
        self.nnTanh.train(self.error)

        self.assertAlmostEqual(0.2584350302004842, self.nnTanh.forward_propagation().get_net_error(None),
                               msg="Network error after single training incorrect.")

    def test_backpropagation_initial_relu(self):
        self.setup_RELU()
        self.nnRELU.train(self.error)

        self.assertAlmostEqual(0.2965745618480223, self.nnRELU.forward_propagation().get_net_error(None),
                               msg="Network error after single training incorrect.")

    def test_backpropagation_1000_trainings_tanh(self):
        self.setup_tanh()
        for i in range(1000):
            error = self.nnTanh.train(self.nnTanh.forward_propagation().get_net_error(None))

        self.assertAlmostEqual(1.1602673305124693e-06, error, msg="Network error after 1000 trainings incorrect.")

    def test_backpropagation_1000_trainings_relu(self):
        self.setup_RELU()
        for i in range(1000):
            error = self.nnRELU.train(self.nnRELU.forward_propagation().get_net_error(None))

        self.assertAlmostEqual(1.504632769052528e-36, error, msg="Network error after 1000 trainings incorrect.")

    def test_backpropagation_1000_trainings_two_hidden_layers(self):
        self.setup_two_hidden_layers()
        for i in range(1000):
            error = self.nn.train(self.nn.forward_propagation().get_net_error(None))

        self.assertAlmostEqual(0.0021487640025111957, error, msg="Network error after 1000 trainings incorrect.")


if __name__ == '__main__':
    unittest.main()
