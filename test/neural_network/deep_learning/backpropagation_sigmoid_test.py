import sys

from methods.neural_network.deep_learning.hidden_neuron_deep_learning import HiddenNeuronDeepLearning
from methods.neural_network.deep_learning.neural_network_deeplearning import NeuralNetworkDeepLearning
from methods.neural_network.deep_learning.output_neuron_deep_learning import OutputNeuronDeepLearning
from methods.neural_network.input_neuron import InputNeuron

sys.path.append("../../..")

from methods.neural_network.deep_learning.sigmoid import Sigmoid
from methods.neural_network.deep_learning.tanh import Tanh
from methods.neural_network.deep_learning.relu import RELU

import unittest


class BackpropagationTest(unittest.TestCase):

    def setUp(self):
        nn = NeuralNetworkDeepLearning(Sigmoid(), 0.5)
        self.nn = nn
        self.i1 = InputNeuron(nn, "I1")
        self.i2 = InputNeuron(nn, "I2")
        self.h1 = HiddenNeuronDeepLearning(nn, "H1").bias(0.35)
        self.h2 = HiddenNeuronDeepLearning(nn, "H2").bias(0.35)
        self.o1 = OutputNeuronDeepLearning(nn, "O1").bias(0.6)
        self.o2 = OutputNeuronDeepLearning(nn, "O2").bias(0.6)

        self.h1.connect_input(self.i1, 0.15)
        self.h1.connect_input(self.i2, 0.2)

        self.h2.connect_input(self.i1, 0.25)
        self.h2.connect_input(self.i2, 0.3)

        self.o1.connect_input(self.h1, 0.4)
        self.o1.connect_input(self.h2, 0.45)

        self.o2.connect_input(self.h1, 0.5)
        self.o2.connect_input(self.h2, 0.55)

        self.nn.compute_layers()
        self.nn.size_input_dataset = 1
        self.error = self.nn.set_inputs(0.05, 0.1).set_target_outputs(0.01, 0.99) \
            .forward_propagation().get_net_error(None)

    def test_backpropagation_second_to_last_layer(self):
        self.nn.train(self.error)

        self.assertEqual(0.35891647971788465, self.nn.input_synapses[self.o1][self.h1],
                         "Weight w5 after initial training incorrect.")
        self.assertEqual(0.4086661860762334, self.nn.input_synapses[self.o1][self.h2],
                         "Weight w6 after initial training incorrect.")
        self.assertEqual(0.5113012702387375, self.nn.input_synapses[self.o2][self.h1],
                         "Weight w7 after initial training incorrect.")
        self.assertEqual(0.5613701211079891, self.nn.input_synapses[self.o2][self.h2],
                         "Weight w8 after initial training incorrect.")

    def test_backpropagation_third_to_last_layer(self):
        self.nn.train(self.error)

        self.assertEqual(0.14981763856120295, self.nn.input_synapses[self.h1][self.i1],
                         "Weight w1 after initial training incorrect.")
        self.assertEqual(0.19963527712240592, self.nn.input_synapses[self.h1][self.i2],
                         "Weight w2 after initial training incorrect.")
        self.assertEqual(0.2497881851977662, self.nn.input_synapses[self.h2][self.i1],
                         "Weight w3 after initial training incorrect.")
        self.assertEqual(0.29957637039553237, self.nn.input_synapses[self.h2][self.i2],
                         "Weight w4 after initial training incorrect.")

    def test_backpropagation_initial_sigmoid(self):
        self.nn.train(self.error)

        self.assertEqual(0.2804840766059972, self.nn.forward_propagation().get_net_error(None),
                         msg="Network error after single training incorrect.")

    def test_backpropagation_1000_trainings_sigmoid(self):
        for i in range(1000):
            error = self.nn.train(self.nn.forward_propagation().get_net_error(None))

        self.assertAlmostEqual(0.00027018697029697943, error, msg="Network error after 1000 trainings incorrect.")


if __name__ == '__main__':
    unittest.main()
