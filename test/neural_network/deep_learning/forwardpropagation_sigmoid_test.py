import sys

from methods.neural_network.deep_learning.hidden_neuron_deep_learning import HiddenNeuronDeepLearning
from methods.neural_network.deep_learning.neural_network_deeplearning import NeuralNetworkDeepLearning
from methods.neural_network.deep_learning.output_neuron_deep_learning import OutputNeuronDeepLearning
from methods.neural_network.input_neuron import InputNeuron

sys.path.append("../../..")
from methods.neural_network.deep_learning.sigmoid import Sigmoid
import unittest


class ForwardpropagationTest(unittest.TestCase):

    def setUp(self):
        nn = NeuralNetworkDeepLearning(Sigmoid())
        self.nn = nn
        self.i1 = InputNeuron(nn, "I1")
        self.i2 = InputNeuron(nn, "I2")
        self.h1 = HiddenNeuronDeepLearning(nn, "H1").bias(0.35)
        self.h2 = HiddenNeuronDeepLearning(nn, "H2").bias(0.35)
        self.o1 = OutputNeuronDeepLearning(nn, "O1").bias(0.6)
        self.o2 = OutputNeuronDeepLearning(nn, "O2").bias(0.6)

        self.h1.connect_input(self.i1, 0.15)
        self.h1.connect_input(self.i2, 0.2)

        self.h2.connect_input(self.i1, 0.25)
        self.h2.connect_input(self.i2, 0.3)

        self.o1.connect_input(self.h1, 0.4)
        self.o1.connect_input(self.h2, 0.45)

        self.o2.connect_input(self.h1, 0.5)
        self.o2.connect_input(self.h2, 0.55)

        self.nn.compute_layers()
        self.nn.size_input_dataset = 1
        self.error = self.nn.set_inputs(0.05, 0.1).set_target_outputs(0.01, 0.99) \
            .forward_propagation().get_net_error(None)

    def test_weights_sigmoid_initial_second_to_last_layer(self):
        self.assertEqual(0.4, self.nn.input_synapses[self.o1][self.h1],
                         "Weight w5 after setup incorrect.")
        self.assertEqual(0.45, self.nn.input_synapses[self.o1][self.h2],
                         "Weight w6 after setup incorrect.")
        self.assertEqual(0.5, self.nn.input_synapses[self.o2][self.h1],
                         "Weight w7 after setup incorrect.")
        self.assertEqual(0.55, self.nn.input_synapses[self.o2][self.h2],
                         "Weight w8 after setup incorrect.")

    def test_weights_setup_third_to_last_layer(self):
        self.assertEqual(0.15, self.nn.input_synapses[self.h1][self.i1],
                         "Weight w1 after setup incorrect.")
        self.assertEqual(0.2, self.nn.input_synapses[self.h1][self.i2],
                         "Weight w2 after setup incorrect.")
        self.assertEqual(0.25, self.nn.input_synapses[self.h2][self.i1],
                         "Weight w3 after setup incorrect.")
        self.assertEqual(0.3, self.nn.input_synapses[self.h2][self.i2],
                         "Weight w4 after setup incorrect.")

    def test_outputs_after_forward_propagation(self):
        self.assertEqual(0.7513650695523157, self.o1.get_output(), "Output o1 after forward propagation incorrect.")
        self.assertEqual(0.7729284653214625, self.o2.get_output(), "Output o2 after forward propagation incorrect.")

    def test_get_neterror_after_forward_propagation(self):
        self.assertEqual(0.2983711087600027, self.error, "Initial Network error incorrect (sigmoid).")

    def test_error_o1_after_forward_propagation(self):
        self.assertEqual(0.274811083176155, self.nn.get_error(self.o1))

    def test_error_o2_after_forward_propagation(self):
        self.assertEqual(0.023560025583847746, self.nn.get_error(self.o2))


if __name__ == '__main__':
    unittest.main()
