import unittest

from methods.util.output_check import OutputCheck


class OutputCheckTest(unittest.TestCase):
    def setUp(self):
        self.output_check = OutputCheck()

    def test_add_computed_and_to_train(self):
        self.setUp()
        self.output_check.add_computed_and_to_train(True, True)
        self.assertTrue(len(self.output_check.value_list) > 0)

    def test_all_non_equal_values(self):
        self.setUp()
        self.output_check.add_computed_and_to_train(True, True)
        self.output_check.add_computed_and_to_train(True, False)
        self.output_check.add_computed_and_to_train(False, True)
        self.output_check.add_computed_and_to_train(False, False)
        self.assertTrue(self.output_check.all_non_equal_values() == 2)

    def test_check_if_equal(self):
        self.setUp()
        self.output_check.add_computed_and_to_train(True, True)
        self.assertTrue(self.output_check.value_list[0].check_if_equal())


if __name__ == '__main__':
    unittest.main()
