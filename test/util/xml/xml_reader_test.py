import unittest
from methods.util.xml.XMLReader import *
from methods.neural_network.neural_network import NeuralNetwork


class XMLReaderTest(unittest.TestCase):

    def test_read(self):
        neural_network: NeuralNetwork = XMLReader.loadNetwork('util/xml/nn-test.xml')

        self.assertEqual(len(neural_network.input_neurons), 2)
        self.assertEqual(len(neural_network.hidden_neurons), 2)
        self.assertEqual(len(neural_network.output_neurons), 1)

        self.assertEqual(neural_network.output_neurons[0].id, '5')


if __name__ == '__main__':
    unittest.main()
