from methods.util.xml.XMLWriter import XMLWriter
from methods.neural_network.hill_climbing.neural_network_hill_climbing import NeuralNetworkHillClimbing
from methods.neural_network.hill_climbing.neural_network_hill_climbing import NeuralNetwork
from methods.neural_network.neuron import Neuron
import unittest
import mockito
import os


def compare_file(file1, file2):
    identical = True
    f1 = open(file1, "r")
    f2 = open(file2, "r")
    for line1 in f1:
        for line2 in f2:
            if line1 != line2:
                identical = False
            break
    f1.close()
    f2.close()
    return identical


def create_neuron(id, bias, fired, output):
    neuron: Neuron = mockito.mock(Neuron)
    mockito.when(neuron).get_id().thenReturn(id)
    mockito.when(neuron).get_bias().thenReturn(bias)
    mockito.when(neuron).get_fired().thenReturn(fired)
    mockito.when(neuron).compute_output().thenReturn(output)
    return neuron


class XMLWriterTest(unittest.TestCase):

    def test_write(self):
        network: NeuralNetwork = NeuralNetworkHillClimbing()

        input_neuron_1: Neuron = create_neuron("1", 0.0, True, 1.0)
        input_neuron_2: Neuron = create_neuron("2", 0.0, True, 1.0)

        hidden_neuron_1: Neuron = create_neuron("3", -0.1234, True, 0.5)
        hidden_neuron_2: Neuron = create_neuron("4", 0.1234, False, 1.5)

        output_neuron: Neuron = create_neuron("5", 0.0, True, 0.23)

        network.add_or_change_synapse(input_neuron_1, hidden_neuron_1, -0.11)
        network.add_or_change_synapse(input_neuron_2, hidden_neuron_1, 0.22)
        network.add_or_change_synapse(input_neuron_1, hidden_neuron_2, 0.33)
        network.add_or_change_synapse(hidden_neuron_1, output_neuron, 0.44)
        network.add_or_change_synapse(hidden_neuron_2, output_neuron, 0.55)

        network.input_neurons = [input_neuron_1, input_neuron_2]
        network.hidden_neurons = [hidden_neuron_1, hidden_neuron_2]
        network.output_neurons = [output_neuron]

        XMLWriter.saveNetwork(network, 'util/xml/nn-test-compare.xml')

        self.assertTrue(compare_file('util/xml/nn-test-compare.xml', 'util/xml/nn-test.xml'))

        os.remove('util/xml/nn-test-compare.xml')


if __name__ == '__main__':
    unittest.main()
