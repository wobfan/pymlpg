import unittest

from methods.util.box import Box


class OutputCheckTest(unittest.TestCase):
    def test_add_computed_and_to_train(self):
        box = Box(0.5, 0, 1.5, 1)
        self.assertEquals(0.5, box.x1)
        self.assertEquals(1.5, box.x2)
        self.assertEquals(0, box.y1)
        self.assertEquals(1, box.y2)
        self.assertIsNotNone(box)


if __name__ == '__main__':
    unittest.main()
