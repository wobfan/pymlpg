import sys

from methods.util import manage_nn
from methods.util.labelled_points_and_boxes import LabelledPointsAndBoxes

sys.path.append("../../..")

from methods.neural_network.neuron import Neuron
import unittest
import mockito


class ManageNNTest(unittest.TestCase):

    @staticmethod
    def create_neuron(id, bias, fired, output):
        neuron: Neuron = mockito.mock(Neuron)
        mockito.when(neuron).get_id().thenReturn(id)
        mockito.when(neuron).get_bias().thenReturn(bias)
        mockito.when(neuron).get_fired().thenReturn(fired)
        mockito.when(neuron).compute_output().thenReturn(output)
        return neuron

    @staticmethod
    def get_training_set(number):
        n: Neuron = ManageNNTest.create_neuron("0", 0.56, False, 0.34)
        training_points: LabelledPointsAndBoxes = LabelledPointsAndBoxes(number, n, equidistant=True)

        training_set = []
        training_set.append(training_points)
        return training_set

    def test_count_training_set_entries(self):
        assert 400 == manage_nn.count_training_set_entries(self.get_training_set(400))
