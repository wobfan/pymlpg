import sys

from methods.util.labelled_point import LabelledPoint
from methods.util.labelled_points_and_boxes import LabelledPointsAndBoxes

sys.path.append("../../..")

from methods.neural_network.neuron import Neuron
import unittest
import mockito


class LabelledPointsAndBoxesTest(unittest.TestCase):

    neuron: Neuron = None

    labelled_point: LabelledPoint = None

    labelled_points_and_boxes: LabelledPointsAndBoxes = None

    @staticmethod
    def create_neuron(id, bias, fired, output):
        neuron: Neuron = mockito.mock(Neuron)
        mockito.when(neuron).get_id().thenReturn(id)
        mockito.when(neuron).get_bias().thenReturn(bias)
        mockito.when(neuron).get_fired().thenReturn(fired)
        mockito.when(neuron).compute_output().thenReturn(output)
        return neuron

    @staticmethod
    def create_labelled_point(x1, x2):
        labelled_point: LabelledPoint = LabelledPoint(x1, x2, None)
        return labelled_point

    def setUp(self):
        self.neuron = self.create_neuron("1", 0.31, True, 0.21)

    def test_add_labelled_point(self):
        self.labelled_points_and_boxes = LabelledPointsAndBoxes()
        self.labelled_point = self.create_labelled_point(1.0, 3.0)
        self.labelled_points_and_boxes.labelled_points  # noqa
        self.labelled_points_and_boxes.labelled_points.append(self.labelled_point)
        assert 1 == len(self.labelled_points_and_boxes.labelled_points)

    def test_create_labelled_points_without_equidistant_test(self):
        self.labelled_points_and_boxes = LabelledPointsAndBoxes(400, self.neuron)
        assert self.labelled_points_and_boxes is not None
