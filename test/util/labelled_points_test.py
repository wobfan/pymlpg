import sys

from methods.util.labelled_point import LabelledPoint
from methods.util.labelled_points_and_boxes import LabelledPointsAndBoxes

sys.path.append("../../..")

from methods.neural_network.neuron import Neuron
from methods.util.box import Box
import unittest
import mockito


class LabelledPointTest(unittest.TestCase):

    labelled_point: LabelledPoint = None

    @staticmethod
    def create_neuron(id, bias, fired, output):
        neuron: Neuron = mockito.mock(Neuron)
        mockito.when(neuron).get_id().thenReturn(id)
        mockito.when(neuron).get_bias().thenReturn(bias)
        mockito.when(neuron).get_fired().thenReturn(fired)
        mockito.when(neuron).compute_output().thenReturn(output)
        return neuron

    @staticmethod
    def create_box(x1, x2, y1, y2):
        box: Box = Box(x1, y1, x2, y2)
        return box

    def test_create_labelled_point(self):
        n: Neuron = self.create_neuron("0", 0.56, False, 0.34)
        self.labelled_point = LabelledPoint(1.0, 2.0, n, -1.0)

        assert 1.0 == self.labelled_point.x
        assert 2.0 == self.labelled_point.y
        assert -1.0 == self.labelled_point.label
        assert n == self.labelled_point.neuron

    def test_create_null_labelled_point(self):
        n: Neuron = self.create_neuron("0", 0.56, False, 0.34)
        self.labelled_point = LabelledPoint(None, None, n)

        assert self.labelled_point.x is not None
        assert self.labelled_point.y is not None

    def test_create_labelled_point_inside_box(self):
        n: Neuron = self.create_neuron("0", 0.56, False, 0.34)
        box: Box = self.create_box(0.2, 0.4, 0.2, 0.4)
        LabelledPointsAndBoxes.boxes.append(box)

        self.labelled_point = LabelledPoint(0.3, 0.3, n)

        assert 1.0 == self.labelled_point.label

    def test_create_labelled_point_outside_box(self):
        n: Neuron = self.create_neuron("0", 0.56, False, 0.34)
        box: Box = self.create_box(0.2, 0.4, 0.2, 0.4)
        LabelledPointsAndBoxes.boxes.append(box)

        self.labelled_point = LabelledPoint(0.1, 0.05, n)
        assert 0.0 == self.labelled_point.label
