[![pipeline status](https://gitlab.com/wobfan/pymlpg/badges/master/pipeline.svg)](https://gitlab.com/wobfan/pymlpg/-/commits/master) [![coverage report](https://gitlab.com/wobfan/pymlpg/badges/master/coverage.svg)](https://gitlab.com/wobfan/pymlpg/-/commits/master)

# Project Purpose

This project is a(n implementation) playground to investigate and understand machine learning algorithmus from scratch.
pyMPLG is a port of the original [MLPG Project](https://gitlab.com/lobequadrat/mlpg) written in Java.

# Implementation status

#### *italic points are implemented in the original version but not yet in this port*

- Neural Network (NN) Core API:
    - Topology definition, i.e., input nodes, hidden nodes, output nodes and connections
    - Simulation (fired or not) & training
    - *Visualization*
        - *Output CSV + Scatterplot*
    - *XML serialization*
    - Neuron Variants:
        - Perceptrons Network: https://en.wikipedia.org/wiki/Perceptron
            - discrete switching behavior
            - training with Hill Climbing
        - Deep Neural Networks with
            - Activation Functions (Sigmoid)
            - training with Backpropagation
    - Neural Network Classes:
        - Feedforward NN


- Examples:
    - Boolean Algebra
        - AND
        - OR
        - NOT
        - XOR
    - Concert visit
    - Threshold
    - *Box / Area*

# get started

The code is written for ```Python 3.8```. 
Examples are located in the main folder of this project, and can be run bysimply running ```python3 AND.py```.

[onboarding wiki](https://gitlab.com/wobfan/pymlpg/-/wikis/Onboarding)

# License

[Eclipse Public License 2.0](https://www.eclipse.org/legal/epl-2.0/)

# Contributors

- Marcel Schumacher (marcelschumacher94@gmx.de)
- Stefan Wobbe (wobfan@pm.me)
- Max Hoefinghoff
- Ralf Buschermöhle (buschermoehle@protonmail.com)
- Daniel Süpke (daniel@suepke.net)

Contributers welcome - please contact via email.
